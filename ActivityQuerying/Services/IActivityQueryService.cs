﻿using ActivityQuerying.Filtering;
using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public interface IActivityQueryService
    {
        Task<Activity> GetRandomActivityAsync();

        Task<Activity> GetRandomActivityAsync(ActivityFilter activityFilter);
    }
}
