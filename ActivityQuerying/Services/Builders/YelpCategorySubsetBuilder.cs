﻿using ActivityQuerying.Models.Yelp;
using ActivityQuerying.Services.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Builders
{
    public class YelpCategorySubsetBuilder : IYelpCategorySubsetBuilder
    {
        private string _categoryAlias;

        private bool _excludeAllChildrenByDefault = false;

        private List<string> _includedCategoryAliases = new List<string>();
        private List<string> _excludedCategoryAliases = new List<string>();

        private readonly YelpCategoryCache _yelpCategoryCache;

        public YelpCategorySubsetBuilder(YelpCategoryCache yelpCategoryCache)
        {
            _yelpCategoryCache = yelpCategoryCache;
        }

        public IYelpCategorySubsetBuilder Create(string categoryAlias)
        {
            _includedCategoryAliases = new List<string>();
            _excludedCategoryAliases = new List<string>();

            _categoryAlias = categoryAlias;

            return this;
        }

        public IYelpCategorySubsetBuilder IncludeChildCategory(string categoryAlias)
        {
            _includedCategoryAliases.Add(categoryAlias);

            return this;
        }

        public IYelpCategorySubsetBuilder ExcludeChildCategory(string categoryAlias)
        {
            _excludedCategoryAliases.Add(categoryAlias);

            return this;
        }

        public IYelpCategorySubsetBuilder ExcludeAllChildCategories()
        {
            _excludeAllChildrenByDefault = true;

            return this;
        }

        public async Task<YelpCategory> BuildAsync()
        {
            var category = this.GetYelpCategoryCopy(await _yelpCategoryCache.GetCategoryByAliasAsync(_categoryAlias));

            if (_excludeAllChildrenByDefault)
                category.ChildCategories.Clear();

            await this.AddIncludedCategoriesAsync(category);

            this.RemoveExcludedCategories(category);

            return category;
        }

        private async Task AddIncludedCategoriesAsync(YelpCategory category)
        {
            foreach (var includedCategoryAlias in _includedCategoryAliases)
            {
                var yelpCategory = this.GetYelpCategoryCopy(await _yelpCategoryCache.GetCategoryByAliasAsync(includedCategoryAlias));

                category.ChildCategories.Add(yelpCategory);
            }
        }

        private void RemoveExcludedCategories(YelpCategory category)
        {
            foreach (var excludedCategoryAlias in _excludedCategoryAliases)
                this.ExcludeCategory(category, excludedCategoryAlias);
        }

        private void ExcludeCategory(YelpCategory parentYelpCategory, string categoryAlias)
        {
            var index = parentYelpCategory.ChildCategories.FindIndex(c => c.Alias == categoryAlias);

            if (index >= 0)
            {
                parentYelpCategory.ChildCategories.RemoveAt(index);

                return;
            }

            parentYelpCategory.ChildCategories.ForEach(c => this.ExcludeCategory(c, categoryAlias));
        }

        private YelpCategory GetYelpCategoryCopy(YelpCategory toCopy)
        {
            var copy = new YelpCategory
            {
                Alias = toCopy.Alias,
                Title = toCopy.Title,
                CountryBlacklist = toCopy.CountryBlacklist,
                CountryWhitelist = toCopy.CountryWhitelist,
            };

            foreach (var childCategory in toCopy.ChildCategories)
            {
                var childCategoryCopy = this.GetYelpCategoryCopy(childCategory);

                copy.ChildCategories.Add(childCategoryCopy);
            }

            return copy;
        }
    }
}
