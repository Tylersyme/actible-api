﻿using ActivityQuerying.Models.Yelp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Builders
{
    public interface IYelpCategorySubsetBuilder
    {
        IYelpCategorySubsetBuilder Create(string categoryAlias);

        IYelpCategorySubsetBuilder IncludeChildCategory(string categoryAlias);

        IYelpCategorySubsetBuilder ExcludeChildCategory(string categoryAlias);

        IYelpCategorySubsetBuilder ExcludeAllChildCategories();

        Task<YelpCategory> BuildAsync();
    }
}
