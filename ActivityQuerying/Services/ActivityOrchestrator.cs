﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Models.Yelp;
using ActivityQuerying.Services.ActivityResults;
using Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public class ActivityOrchestrator : IActivityOrchestrator
    {
        private readonly IUserViewedActivitySourceMapRepository _userViewedActivitySourceMapRepository;

        private readonly IActivityResultService<YelpActivityResult, YelpActivityResultSummary> _yelpActivityResultService;

        public ActivityOrchestrator(
            IUserViewedActivitySourceMapRepository userViewedActivitySourceMapRepository,
            IActivityResultService<YelpActivityResult, YelpActivityResultSummary> yelpActivityResultService)
        {
            _userViewedActivitySourceMapRepository = userViewedActivitySourceMapRepository;

            _yelpActivityResultService = yelpActivityResultService;
        }

        public async Task<ActivityBatch> GetNextActivityBatchAsync(Guid userId, ActivityFilter activityFilter, ActivityHistory activityHistory)
        {
            var yelpActivityResult = await _yelpActivityResultService.GetActivityResultAsync(activityFilter, activityHistory);

            var yelpActivityResultSummary = _yelpActivityResultService.MapActivityResultSummary(yelpActivityResult);

            var totalActivitiesToExclude = yelpActivityResult.TotalPossibleResults / 5;

            var filteredActivities = await this.ExcludeRecentlyViewedActivitiesAsync(userId, yelpActivityResult.Activities, totalActivitiesToExclude);

            yelpActivityResult.Activities = filteredActivities;

            return new ActivityBatch
            {
                ActivityResults = new List<ActivityResult>
                {
                    yelpActivityResult,
                },
                ActivityResultSummary = new List<ActivityResultSummary>
                {
                    yelpActivityResultSummary,
                },
            };
        }

        /// <summary>
        /// Removes any activities which have already been viewed by the user recently.
        /// </summary>
        /// <param name="activities">The activities which may have already been viewed.</param>
        /// <returns>The activities which have not been recently viewed.</returns>
        private async Task<List<Activity>> ExcludeRecentlyViewedActivitiesAsync(Guid userId, List<Activity> activities, int totalToExclude)
        {
            var mostRecentActivitySources = await _userViewedActivitySourceMapRepository.GetMostRecentActivitySourcesAsync(userId, totalToExclude);

            return activities
                .Where(a => !mostRecentActivitySources.Any(s => s.ActivityIdentifier == a.Source.Identifier))
                .ToList();
        }
    }
}
