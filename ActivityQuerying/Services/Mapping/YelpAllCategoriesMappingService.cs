﻿using ActivityQuerying.Models.Yelp;
using ActivityQuerying.Services.Mapping;
using Org.BouncyCastle.Crypto.Digests;
using PlacesQuerying.Responses.Yelp.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Services.Mapping
{
    public class YelpAllCategoriesMappingService : IYelpAllCategoriesMappingService
    {
        public List<Models.Yelp.YelpCategory> Map(YelpAllCategoriesResponse response)
        {
            var allResponseCategories = response.Categories;

            var mappedYelpCategories = new List<Models.Yelp.YelpCategory>();

            foreach (var responseCategory in allResponseCategories)
            {
                var mappedYelpCategory = GetMappedCategoryOrAdd(responseCategory);

                // If the category is a topmost parent then is cannot add itself to the children of other categories
                if (!this.IsResponseCategoryChild(responseCategory))
                    continue;

                AddChildCategoryToParents(responseCategory.ParentAliases, mappedYelpCategory);
            }

            return mappedYelpCategories;

            #region LocalFunctions

            // Will add the given category to the child list of all its parents.
            // The parent category is created if it does not already exist.
            void AddChildCategoryToParents(List<string> parentAliases, Models.Yelp.YelpCategory childMappedCategory)
            {
                foreach (var parentAlias in parentAliases)
                {
                    var parentResponseCategory = allResponseCategories.First(c => c.Alias == parentAlias);

                    var mappedCategory = GetMappedCategoryOrAdd(parentResponseCategory);

                    mappedCategory.ChildCategories.Add(childMappedCategory);

                    childMappedCategory.Parent = mappedCategory;
                }
            }

            // Returns the category if it has already been mapped (based upon aliases).
            // Otherwise the category is mapped and added and the newly created category is returned.
            Models.Yelp.YelpCategory GetMappedCategoryOrAdd(PlacesQuerying.Responses.Yelp.Category.YelpCategory responseCategory)
            {
                var existingMappedCategory = mappedYelpCategories.FirstOrDefault(c => c.Alias == responseCategory.Alias);

                if (existingMappedCategory != null)
                    return existingMappedCategory;

                var mappedYelpCategory = new Models.Yelp.YelpCategory
                {
                    Alias = responseCategory.Alias,
                    Title = responseCategory.Title,
                    CountryBlacklist = responseCategory.CountryBlacklist,
                    CountryWhitelist = responseCategory.CountryWhitelist,
                };

                mappedYelpCategories.Add(mappedYelpCategory);

                return mappedYelpCategory;
            }

            #endregion
        }

        private bool IsResponseCategoryChild(PlacesQuerying.Responses.Yelp.Category.YelpCategory responseCategory) =>
            responseCategory.ParentAliases.Any();
    }
}
