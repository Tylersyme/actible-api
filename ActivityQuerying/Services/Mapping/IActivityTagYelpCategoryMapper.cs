﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models.Yelp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Mapping
{
    public interface IActivityTagYelpCategoryMapper
    {
        Task<List<YelpCategory>> GetCategoriesAsync(ActivityTag activityTag);
    }
}
