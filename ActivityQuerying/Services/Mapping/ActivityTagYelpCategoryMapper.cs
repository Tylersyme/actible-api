﻿using ActivityQuerying.Enums;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models.Yelp;
using ActivityQuerying.Services.Builders;
using ActivityQuerying.Services.Cache;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Mapping
{
    public class ActivityTagYelpCategoryMapper : IActivityTagYelpCategoryMapper
    {
        private IDictionary<ActivityTag, List<YelpCategory>> YELP_CATEGORY_MAPPINGS = new Dictionary<ActivityTag, List<YelpCategory>>();

        private bool _hasCategoryMappingInitialized = false;

        private readonly IYelpCategorySubsetBuilder _yelpCategorySubsetBuilder;

        private readonly YelpCategoryCache _yelpCategoryCache;

        public ActivityTagYelpCategoryMapper(
            IYelpCategorySubsetBuilder yelpCategorySubsetBuilder,
            YelpCategoryCache yelpCategoryCache)
        {
            _yelpCategorySubsetBuilder = yelpCategorySubsetBuilder;
            _yelpCategoryCache = yelpCategoryCache;
        }

        public async Task<List<YelpCategory>> GetCategoriesAsync(ActivityTag activityTag)
        {
            await this.EnstureYelpCategoryMappingsInitializedAsync();

            return YELP_CATEGORY_MAPPINGS[activityTag];
        }

        private async Task EnstureYelpCategoryMappingsInitializedAsync()
        {
            if (_hasCategoryMappingInitialized)
                return;

            await this.InitializeYelpCategoryMappingsAsync();
        }

        private async Task InitializeYelpCategoryMappingsAsync()
        {
            _hasCategoryMappingInitialized = true;

            var categoryMappings = new Dictionary<ActivityTag, List<YelpCategory>>
            {
                [ActivityTag.Active] = new List<YelpCategory>
                {
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.ActiveLife.Alias)
                        .ExcludeChildCategory("fitness")
                        .ExcludeChildCategory("amateursportsteams")
                        .ExcludeChildCategory("nudist")
                        .ExcludeChildCategory("seniorcenters")
                        .ExcludeChildCategory("sports_clubs")
                        .BuildAsync(),
                },
                [ActivityTag.Food] = new List<YelpCategory>
                {
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.Restaurants.Alias)
                        .BuildAsync(),
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.ArtsAndEntertainment.Alias)
                        .ExcludeAllChildCategories()
                        .IncludeChildCategory("eatertainment")
                        .IncludeChildCategory("generalfestivals")
                        .IncludeChildCategory("funfair")
                        .IncludeChildCategory("wineries")
                        .BuildAsync(),
                },
                [ActivityTag.WalkAndView] = new List<YelpCategory>
                {
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.ArtsAndEntertainment.Alias)
                        .ExcludeAllChildCategories()
                        .IncludeChildCategory("galleries")
                        .IncludeChildCategory("gardens")
                        .IncludeChildCategory("castles")
                        .IncludeChildCategory("culturalcenter")
                        .IncludeChildCategory("farms")
                        .IncludeChildCategory("festivals")
                        .IncludeChildCategory("hauntedhouses")
                        .IncludeChildCategory("museums")
                        .IncludeChildCategory("observatories")
                        .IncludeChildCategory("planetarium")
                        .IncludeChildCategory("streetart")
                        .BuildAsync(),
                },
                [ActivityTag.Show] = new List<YelpCategory>
                {
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.ArtsAndEntertainment.Alias)
                        .ExcludeAllChildCategories()
                        .IncludeChildCategory("movietheaters")
                        .IncludeChildCategory("jazzandblues")
                        .IncludeChildCategory("marchingbands")
                        .IncludeChildCategory("musicvenues")
                        .IncludeChildCategory("theater")
                        .IncludeChildCategory("sportsteams")
                        .IncludeChildCategory("racetracks")
                        .IncludeChildCategory("rodeo")
                        .IncludeChildCategory("stadiumsarenas")
                        .IncludeChildCategory("tablaoflamenco")
                        .IncludeChildCategory("choirs")
                        .IncludeChildCategory("opera")
                        .BuildAsync(),
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.Nightlife.Alias)
                        .ExcludeAllChildCategories()
                        .IncludeChildCategory("comedyclubs")
                        .IncludeChildCategory("karaoke")
                        .BuildAsync(),
                },
                [ActivityTag.Relaxing] = new List<YelpCategory>
                {
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.BeautyAndSpas.Alias)
                        .ExcludeAllChildCategories()
                        .IncludeChildCategory("spas")
                        .IncludeChildCategory("massage")
                        .IncludeChildCategory("hotsprings")
                        .BuildAsync(),
                },
                [ActivityTag.Beauty] = new List<YelpCategory>
                {
                    await _yelpCategorySubsetBuilder
                        .Create(YelpTopLevelCategory.BeautyAndSpas.Alias)
                        .ExcludeAllChildCategories()
                        .IncludeChildCategory("eyebrowservices")
                        .IncludeChildCategory("eyelashservice")
                        .IncludeChildCategory("hair")
                        .IncludeChildCategory("makeupartists")
                        .IncludeChildCategory("othersalons")
                        .IncludeChildCategory("perfume")
                        .IncludeChildCategory("piercing")
                        .IncludeChildCategory("permanentmakeup")
                        .IncludeChildCategory("tanning")
                        .BuildAsync(),
                }
            };

            foreach (var categoryMappingEntry in categoryMappings)
            {
                var generalCategories = new List<YelpCategory>();

                foreach (var category in categoryMappingEntry.Value)
                    generalCategories.AddRange(await this.GetMostGeneralCategoriesAsync(category));

                YELP_CATEGORY_MAPPINGS.Add(categoryMappingEntry.Key, generalCategories);
            }
        }

        /// <summary>
        /// Goes through the category tree and returns all of the most general categories possible from each parent category.
        /// This is used to prevent including all child categories of a parent when the parent by itself is enough.
        /// If no children were removed from the parent category, then only the parent category is needed since it
        /// represents all of its children at once.
        /// In the case where a parent has exclude children, the parent should not be included since it represents the aggregate of ALL its children.
        /// As a result, when one or more children are excluded, that parent category can no longer be used and its chldren are used instead.
        /// </summary>
        /// <param name="category">The root yelp category that will be traversed.</param>
        /// <returns></returns>
        private async Task<List<YelpCategory>> GetMostGeneralCategoriesAsync(YelpCategory category)
        {
            if (!await IsChildrenExcludedAsync(category))
                return new List<YelpCategory> { category };

            var generalCategories = new List<YelpCategory>();

            foreach (var childCategory in category.ChildCategories)
                generalCategories.AddRange(await this.GetMostGeneralCategoriesAsync(childCategory));

            return generalCategories;

            async Task<bool> IsChildrenExcludedAsync(YelpCategory parentCategory)
            {
                var totalChildren = parentCategory.ChildCategories.Count;

                var totalExpectedChildren = (await _yelpCategoryCache.GetCategoryByAliasAsync(category.Alias)).ChildCategories.Count;

                return totalChildren < totalExpectedChildren;
            }
        }

    }
}
