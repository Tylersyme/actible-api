﻿using ActivityQuerying.Enums;
using PlacesQuerying.Responses.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Services.Mapping.Noun
{
    public interface ITomTomClassificationNounMappingService
    {
        ActivityNounType Map(Classification classification);
    }
}
