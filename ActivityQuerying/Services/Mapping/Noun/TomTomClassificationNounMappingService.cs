﻿using ActivityQuerying.Enums;
using ActivityQuerying.Models;
using PlacesQuerying.Enums;
using PlacesQuerying.Extensions;
using PlacesQuerying.Responses.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Services.Mapping.Noun
{
    public class TomTomClassificationNounMappingService : ITomTomClassificationNounMappingService
    {
        private static readonly IDictionary<TomTomCategoryCode, ActivityNounType> TOM_TOM_CATEGORY_CODE_NOUN_MAP = new Dictionary<TomTomCategoryCode, ActivityNounType>
        {
            [TomTomCategoryCode.AdventureSportsVenue] = ActivityNounType.Outdoors,
            [TomTomCategoryCode.AmusementPark] = ActivityNounType.AmusementPark,
            [TomTomCategoryCode.Beach] = ActivityNounType.Beach,
            [TomTomCategoryCode.CafePub] = ActivityNounType.CafePub,
            [TomTomCategoryCode.CampingGround] = ActivityNounType.CampingGround,
            [TomTomCategoryCode.Casino] = ActivityNounType.Casino,
            [TomTomCategoryCode.Cinema] = ActivityNounType.Cinema,
            [TomTomCategoryCode.ClubAssociation] = ActivityNounType.Club,
            [TomTomCategoryCode.CommunityCenter] = ActivityNounType.RecreationCenter,
            [TomTomCategoryCode.CulturalCenter] = ActivityNounType.RecreationCenter,
            [TomTomCategoryCode.DepartmentStore] = ActivityNounType.Store,
            [TomTomCategoryCode.ExhibitionConventionCenter] = ActivityNounType.Convention,
            [TomTomCategoryCode.GolfCourse] = ActivityNounType.GolfCourse,
            [TomTomCategoryCode.IceSkatingRink] = ActivityNounType.IceSkating,
            [TomTomCategoryCode.ImportantTouristAttraction] = ActivityNounType.TouristAttraction,
            [TomTomCategoryCode.Library] = ActivityNounType.Library,
            [TomTomCategoryCode.Market] = ActivityNounType.Store,
            [TomTomCategoryCode.Museum] = ActivityNounType.Museum,
            [TomTomCategoryCode.ParkRecreationArea] = ActivityNounType.Park,
            [TomTomCategoryCode.PlaceOfWorship] = ActivityNounType.PlaceOfWorship,
            [TomTomCategoryCode.Restaurant] = ActivityNounType.Restaurant,
            [TomTomCategoryCode.ScenicParnoramicView] = ActivityNounType.PanoramicView,
            [TomTomCategoryCode.Shop] = ActivityNounType.Store,
            [TomTomCategoryCode.ShoppingCenter] = ActivityNounType.Store,
            [TomTomCategoryCode.SportsCenter] = ActivityNounType.SportsCenter,
            [TomTomCategoryCode.Stadium] = ActivityNounType.Stadium,
            [TomTomCategoryCode.SwimmingPool] = ActivityNounType.SwimmingPool,
            [TomTomCategoryCode.TennisCourt] = ActivityNounType.SportsCourt,
            [TomTomCategoryCode.Theater] = ActivityNounType.Theater,
            [TomTomCategoryCode.TrailSystem] = ActivityNounType.Trail,
            [TomTomCategoryCode.Winery] = ActivityNounType.Winery,
        };

        public ActivityNounType Map(Classification classification)
        {
            if (TOM_TOM_CATEGORY_CODE_NOUN_MAP.ContainsKey(classification.Code))
                return TOM_TOM_CATEGORY_CODE_NOUN_MAP[classification.Code];

            if (classification.Code == TomTomCategoryCode.LeisureCenter)
                return this.MapLesiureCenter(classification);
            else if (classification.Code == TomTomCategoryCode.Nightlife)
                return this.MapNightlife(classification);
            else if (classification.Code == TomTomCategoryCode.ZoosArboretaBotanicalGarden)
                return this.MapZoosArboretumBotanicalGarden(classification);

            throw new ArgumentException();
        }
        
        private ActivityNounType MapZoosArboretumBotanicalGarden(Classification classification)
        {
            if (classification.SubcategoryNameContains("zoo"))
                return ActivityNounType.Zoo;
            else if (classification.SubcategoryNameContains("garden"))
                return ActivityNounType.Garden;
            else if (classification.SubcategoryNameContains("wild"))
                return ActivityNounType.WildlifePark;
            else
                return ActivityNounType.Zoo;
        }

        private ActivityNounType MapNightlife(Classification classification)
        {
            if (classification.SubcategoryNameContains("bar"))
                return ActivityNounType.Bar;
            else if (classification.SubcategoryNameContains("comedy"))
                return ActivityNounType.ComedyClub;
            else if (classification.SubcategoryNameContains("club"))
                return ActivityNounType.Club;
            else
                return ActivityNounType.Bar;
        }

        private ActivityNounType MapLesiureCenter(Classification classification)
        {
            if (classification.SubcategoryNameContains("bowling"))
                return ActivityNounType.Bowling;
            else if (classification.SubcategoryNameContainsAny("spa", "sauna"))
                return ActivityNounType.Spa;
            else
                return ActivityNounType.LeisureCenter;
        }
    }
}
