﻿using PlacesQuerying.Responses.Yelp.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Services.Mapping
{
    public interface IYelpAllCategoriesMappingService
    {
        List<Models.Yelp.YelpCategory> Map(YelpAllCategoriesResponse response);
    }
}
