﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models.Yelp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public interface IYelpCategorySelector
    {
        Task<IEnumerable<YelpCategory>> GetCategoriesAsync(ActivityFilter activityFilter);

        Task<IEnumerable<YelpCategory>> GetRandomCategorySubsetAsync();

        Task<bool> IsValidCategoryAsync(string categoryAlias);
    }
}
