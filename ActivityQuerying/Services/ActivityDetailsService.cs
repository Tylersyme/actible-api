﻿using ActivityQuerying.Enums;
using ActivityQuerying.Exceptions;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Models.Components;
using Entities.Enums;
using PlacesQuerying.Requests.Yelp.Search;
using PlacesQuerying.Services.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public class ActivityDetailsService : IActivityDetailsService
    {
        private readonly IYelpClient _yelpClient;

        public ActivityDetailsService(IYelpClient yelpClient)
        {
            _yelpClient = yelpClient;
        }

        public async Task<List<Component>> GetDetailComponentsAsync(ActivityDetailsFilter filter)
        {
            if (filter.ActivitySourceType != ActivitySourceType.Yelp)
                return new List<Component>();

            var detailComponents = new List<Component>();

            return detailComponents;

            //var yelpMatchRequest = new YelpBusinessMatchRequest
            //{
            //    ApiKey = "KZeCdk0pezc_pKs9OopXWk5PD5cqi8onMSbSlgkMvB-xc1E_VKXICNzek0CXzmw5fT-nhw5uLxCdxywzWRgtzve7tNuQI_5B-jz0u_eOS-QDHCapP98pN9UtJVgnX3Yx",
            //    Version = "v3",
            //    Name = filter.Name,
            //    Address1 = filter.Address,
            //    City = filter.City,
            //    Country = filter.Country,
            //    State = filter.State,
            //};

            //var yelpMatchResponse = await _yelpClient.BusinessMatchAsync(yelpMatchRequest);

            var yelpSearchRequest = new YelpBusinessSearchRequest
            {
                ApiKey = "KZeCdk0pezc_pKs9OopXWk5PD5cqi8onMSbSlgkMvB-xc1E_VKXICNzek0CXzmw5fT-nhw5uLxCdxywzWRgtzve7tNuQI_5B-jz0u_eOS-QDHCapP98pN9UtJVgnX3Yx",
                Version = "v3",
                Term = filter.Name,
                Latitude = filter.Latitude,
                Longitude = filter.Longitude,
                Radius = 3000,
            };

            var yelpSearchResponse = await _yelpClient.BusinessSearchAsync(yelpSearchRequest);

            var place = yelpSearchResponse.BusinessResults.Where(b => !b.IsPermanentlyClosed).FirstOrDefault()
                ?? throw new DetailsSearchResultsNotFoundException();

            var placeReviewsComponent = new PlaceReviewsComponent
            {
                PlaceReviewSource = PlaceReviewSource.Yelp,
                Rating = place.Rating,
                ReviewCount = place.ReviewCount,
                ReviewSourceUrl = place.YelpUrl,
            };

            var placeImagesComponent = new ImagesComponent
            {
                ImageUrls = new List<string>
                {
                    place.ImageUrl,
                },
            };

            detailComponents.Add(placeReviewsComponent);
            detailComponents.Add(placeImagesComponent);

            return detailComponents;
        }
    }
}
