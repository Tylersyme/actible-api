﻿using ActivityQuerying.Enums;
using ActivityQuerying.Extensions;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models.Yelp;
using ActivityQuerying.Services.Cache;
using ActivityQuerying.Services.Mapping;
using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public class YelpCategorySelector : IYelpCategorySelector
    {
        public static IReadOnlyList<YelpCategory> VALID_TOP_LEVEL_CATEGORIES { get; private set; } = new List<YelpCategory>();

        private List<YelpCategory> _validCategories = new List<YelpCategory>();

        private Random _random;

        private readonly YelpCategoryCache _yelpCategoryCache;

        private readonly IActivityTagYelpCategoryMapper _activityTagYelpCategoryMapper;

        public YelpCategorySelector(
            YelpCategoryCache yelpCategoryCache,
            IActivityTagYelpCategoryMapper activityTagYelpCategoryMapper)
        {
            _yelpCategoryCache = yelpCategoryCache;

            _activityTagYelpCategoryMapper = activityTagYelpCategoryMapper;

            _random = new Random();

            this.InitializeTopLevelCategoryList();
        }

        public async Task<IEnumerable<YelpCategory>> GetCategoriesAsync(ActivityFilter activityFilter)
        {
            if (!activityFilter.Tags.Any())
                return VALID_TOP_LEVEL_CATEGORIES;

            var yelpCategories = new List<YelpCategory>();

            foreach (var activityTag in activityFilter.Tags)
            {
                var activityTagCategories = await _activityTagYelpCategoryMapper.GetCategoriesAsync(activityTag);

                yelpCategories.AddRange(activityTagCategories);
            }

            return yelpCategories.Distinct();
        }

        public async Task<IEnumerable<YelpCategory>> GetRandomCategorySubsetAsync()
        {
            var validCategories = await this.GetValidYelpCategoriesAsync();

            var randomCategories = new List<YelpCategory>();

            for (var categoryIdx = 0; categoryIdx < validCategories.Count; categoryIdx += _random.Next(1, 10))
                randomCategories.Add(validCategories[categoryIdx]);

            return randomCategories;
        }

        public async Task<bool> IsValidCategoryAsync(string categoryAlias)
        {
            var category = await _yelpCategoryCache.GetCategoryByAliasAsync(categoryAlias);

            return VALID_TOP_LEVEL_CATEGORIES.Any(tc => tc.Alias == category.GetTopmostParent().Alias);
        }

        private void InitializeTopLevelCategoryList()
        {
            var topLevelCategories = new List<YelpTopLevelCategory>
            {
                YelpTopLevelCategory.ActiveLife,
                YelpTopLevelCategory.ArtsAndEntertainment,
                YelpTopLevelCategory.BeautyAndSpas,
                YelpTopLevelCategory.Bicycles,
                YelpTopLevelCategory.Food,
                YelpTopLevelCategory.HotelsAndTravel,
                YelpTopLevelCategory.LocalFlavor,
                YelpTopLevelCategory.Nightlife,
                YelpTopLevelCategory.Pets,
                YelpTopLevelCategory.Restaurants,
            };

            VALID_TOP_LEVEL_CATEGORIES = topLevelCategories
                .Select(async c => await _yelpCategoryCache.GetCategoryByAliasAsync(c.Alias))
                .Select(t => t.Result)
                .ToList();
        }

        private async Task<List<YelpCategory>> GetValidYelpCategoriesAsync()
        {
            if (_validCategories.Any())
                return _validCategories;

            _validCategories = (await _yelpCategoryCache.GetYelpCategoriesAsync())
                .Where(c => VALID_TOP_LEVEL_CATEGORIES.Any(tc => tc.Alias == c.GetTopmostParent().Alias))
                .ToList();

            return _validCategories;
        }
    }
}
