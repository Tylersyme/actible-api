﻿using ActivityQuerying.Models;
using ActivityQuerying.Services.Mapping.Noun;
using PlacesQuerying.Responses.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Transformation
{
    public class TomTomNearbyResponseTransformer : TomTomResponseTransformer<TomTomSearchResult>
    {
        public TomTomNearbyResponseTransformer(ITomTomClassificationNounMappingService nounMappingService) : base(nounMappingService)
        {
        }

        public override Activity Transform(TomTomSearchResult result)
        {
            return base.Transform(result);
        }
    }
}
