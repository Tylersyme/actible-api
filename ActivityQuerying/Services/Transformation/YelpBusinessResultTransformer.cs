﻿using ActivityQuerying.Enums;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Models.Components;
using AutoMapper;
using Entities.Enums;
using PlacesQuerying.Responses.Yelp.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Services.Transformation
{
    public class YelpBusinessResultTransformer : IActivityTransformer<YelpBusinessResult>
    {
        private static readonly long MAXIMUM_CACHE_DURATION_MILLISECONDS = (long)TimeSpan.FromDays(1).TotalMilliseconds;

        private readonly IMapper _mapper;

        public YelpBusinessResultTransformer(IMapper mapper)
        {
            _mapper = mapper;
        }

        public Activity Transform(YelpBusinessResult result)
        {
            var activity = new Activity
            {
                Source = new ActivitySource
                {
                    ActivitySourceType = ActivitySourceType.Yelp,
                    Identifier = result.Id,
                    MaxCacheDurationMilliseconds = MAXIMUM_CACHE_DURATION_MILLISECONDS,
                },
                Title = result.Name,
                Noun = new ActivityNoun
                {
                    Type = ActivityNounType.Restaurant,
                },
                Verb = new ActivityVerb
                {
                    Type = ActivityVerbType.Eat,
                },
            };

            var placeInfoComponent = new PlaceInfoComponent
            {
                Name = result.Name,
                PhoneNumber = result.PhoneNumber,
                WebsiteUrl = result.YelpUrl,
                PriceLevelTitle = result.Price,
                Categories = result.Categories.Select(c => c.Title),
                Distance = result.Distance,
                Coordinates = _mapper.Map<GeolocationCoordinates>(result.Coordinates),
            };

            var placeReviewsComponent = new PlaceReviewsComponent
            {
                PlaceReviewSource = PlaceReviewSource.Yelp,
                Rating = result.Rating,
                ReviewCount = result.ReviewCount,
                ReviewSourceUrl = result.YelpUrl,
            };

            var imagesComponent = new ImagesComponent
            {
                ImageUrls = new List<string>
                {
                    result.ImageUrl,
                },
            };

            var locationComponent = _mapper.Map<LocationComponent>(result.Location);

            activity.Components = new List<Component>
            {
                placeInfoComponent,
                placeReviewsComponent,
                imagesComponent,
                locationComponent,
            };

            return activity;
        }
    }
}
