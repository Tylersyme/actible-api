﻿using ActivityQuerying.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Transformation
{
    public interface IActivityTransformer<TTransorm>
    {
        Activity Transform(TTransorm toTransform);
    }
}
