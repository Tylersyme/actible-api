﻿using ActivityQuerying.Models;
using ActivityQuerying.Models.Components;
using ActivityQuerying.Enums;
using PlacesQuerying.Enums;
using PlacesQuerying.Requests.TomTom.Search;
using PlacesQuerying.Responses.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ActivityQuerying.Services.Mapping.Noun;
using PlacesQuerying.Extensions;

namespace ActivityQuerying.Services.Transformation
{
    public abstract class TomTomResponseTransformer<TTransform> : IActivityTransformer<TTransform>
        where TTransform : TomTomSearchResult
    {
        private readonly ITomTomClassificationNounMappingService _nounMappingService;

        public TomTomResponseTransformer(ITomTomClassificationNounMappingService nounMappingService)
        {
            _nounMappingService = nounMappingService;
        }

        public virtual Activity Transform(TTransform result)
        {
            var classification = result.PointOfInterest.Classifications.First();

            var isShop = result.PointOfInterest.Classifications.Any(c => c.Code == TomTomCategoryCode.Shop);

            var activity = new Activity
            {
                Verb = new ActivityVerb
                {
                    Type = ActivityVerbType.Eat,
                },
                Noun = new ActivityNoun
                {
                    Type = _nounMappingService.Map(classification),
                    Adjectives = classification.GetSubcategoryNames(),
                },
                Title = result.PointOfInterest.Name,
            };

            var placeInfoComponent = new PlaceInfoComponent
            {
                Name = result.PointOfInterest.Name,
                PhoneNumber = result.PointOfInterest.PhoneNumber,
                WebsiteUrl = result.PointOfInterest.WebsiteUrl,
            };

            var lazyPlaceReviewComponent = new LazyPlaceReviewsComponent();

            activity.Components.Add(placeInfoComponent);
            activity.Components.Add(lazyPlaceReviewComponent);

            return activity;
        }
    }
}
