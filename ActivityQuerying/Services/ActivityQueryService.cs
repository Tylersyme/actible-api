﻿using ActivityQuerying.Filtering;
using Entities.Activity;
using GoogleApi;
using GoogleApi.Entities.Places.Search.NearBy.Request;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Persistence.Repositories;
using PlacesQuerying.Enums;
using PlacesQuerying.Requests.TomTom.Search;
using PlacesQuerying.Responses.TomTom.Search;
using PlacesQuerying.Services.Builders;
using PlacesQuerying.Services.Builders.TomTom;
using PlacesQuerying.Services.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public class ActivityQueryService : IActivityQueryService
    {
        private readonly AudlyContext _audlyContext;

        private readonly IActivityRepository _activityRepository;

        private readonly Random _random;

        public ActivityQueryService(
            AudlyContext audlyContext,
            IActivityRepository activityRepository)
        {
            _audlyContext = audlyContext;

            _activityRepository = activityRepository;

            _random = new Random();
        }

        /// <summary>
        /// Returns an activity selected at random.
        /// </summary>
        /// <returns>Random activity.</returns>
        public async Task<Activity> GetRandomActivityAsync() =>
            await this.GetRandomActivityFrom(_audlyContext.Set<Activity>());

        public async Task<Activity> GetRandomActivityAsync(ActivityFilter activityFilter)
        {
            throw new NotImplementedException();

            //var activitiesContainingTags = await _activityRepository.GetActivitiesContainingAnyTagAsync(activityFilter.Tags);

            //var randomIndex = _random.Next(0, activitiesContainingTags.Count);

            //var randomActivity = activitiesContainingTags.Any()
            //    ? activitiesContainingTags[randomIndex]
            //    : await this.GetRandomActivityAsync();

            //return randomActivity;
        }

        private async Task<Activity> GetRandomActivityFrom(IQueryable<Activity> activities)
        {
            var totalActivities = await activities.CountAsync();

            var randomIndex = _random.Next(0, totalActivities);

            var randomActivity = await activities
                .Include(a => a.ActivityGenreMaps)
                    .ThenInclude(agm => agm.Genre)
                .Include(a => a.ActivityTagMaps)
                    .ThenInclude(atm => atm.Tag)
                .Skip(randomIndex)
                .FirstAsync();

            return randomActivity;
        }
    }
}
