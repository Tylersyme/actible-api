﻿using ActivityQuerying.Enums;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Models.Components;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public interface IActivityDetailsService
    {
        Task<List<Component>> GetDetailComponentsAsync(ActivityDetailsFilter filter);
    }
}
