﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Cache
{
    public interface ICache
    {
        bool IsRefreshRequired();

        Task RefreshAsync();
    }
}
