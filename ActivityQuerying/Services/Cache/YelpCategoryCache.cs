﻿using ActivityQuerying.Models.Yelp;
using ActivityQuerying.Services.Mapping;
using PlacesQuerying.Requests.Yelp.Category;
using PlacesQuerying.Services.Clients;
using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Cache
{
    public class YelpCategoryCache : TimeSpanCache
    {
        private readonly IYelpClient _yelpClient;

        private readonly IYelpAllCategoriesMappingService _yelpAllCategoriesMappingService;

        public YelpCategoryCache(
            IYelpClient yelpClient,
            IYelpAllCategoriesMappingService yelpAllCategoriesMappingService)
            : base(TimeSpan.FromDays(7.0))
        {
            _yelpClient = yelpClient;
            _yelpAllCategoriesMappingService = yelpAllCategoriesMappingService;
        }

        private IImmutableDictionary<string, YelpCategory> YelpCategories { get; set; }

        public async Task<IEnumerable<YelpCategory>> GetYelpCategoriesAsync()
        {
            await this.TryRefreshAsync();

            return this.YelpCategories.Values;
        }

        public async Task<YelpCategory> GetCategoryByAliasAsync(string alias)
        {
            await this.TryRefreshAsync();

            if (!this.YelpCategories.ContainsKey(alias))
                throw new KeyNotFoundException($"No yelp category with alias '{alias}' exists.");

            return this.YelpCategories[alias];
        }

        public override async Task RefreshAsync()
        {
            var yelpAllCategoriesRequest = new YelpAllCategoriesRequest
            {
                ApiKey = "KZeCdk0pezc_pKs9OopXWk5PD5cqi8onMSbSlgkMvB-xc1E_VKXICNzek0CXzmw5fT-nhw5uLxCdxywzWRgtzve7tNuQI_5B-jz0u_eOS-QDHCapP98pN9UtJVgnX3Yx",
                Version = "v3",
            };

            var yelpCategories = await _yelpClient.GetAllCategoriesAsync(yelpAllCategoriesRequest);

            var mappedYelpCategories = _yelpAllCategoriesMappingService.Map(yelpCategories);

            this.YelpCategories = mappedYelpCategories.ToImmutableDictionary(k => k.Alias);

            await base.RefreshAsync();
        }

        private async Task TryRefreshAsync()
        {
            if (base.IsRefreshRequired())
                await this.RefreshAsync();
        }
    }
}
