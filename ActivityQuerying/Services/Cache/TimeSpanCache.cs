﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.Cache
{
    public abstract class TimeSpanCache : ICache
    {
        public TimeSpan RefreshInterval { get; private set; }

        public DateTime LastRefresh { get; private set; }

        public TimeSpanCache(TimeSpan refreshInterval)
        {
            this.RefreshInterval = refreshInterval;

            this.LastRefresh = DateTime.MinValue;
        }

        public bool IsRefreshRequired() =>
            this.LastRefresh.Add(this.RefreshInterval) < DateTime.Now;

        public virtual Task RefreshAsync()
        {
            this.LastRefresh = DateTime.Now;

            return Task.CompletedTask;
        }
    }
}
