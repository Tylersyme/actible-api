﻿using ActivityQuerying.Extensions;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Models.Utility;
using ActivityQuerying.Models.Yelp;
using ActivityQuerying.Services.Transformation;
using AutoMapper;
using PlacesQuerying.Requests.Yelp.Search;
using PlacesQuerying.Responses.Yelp.Search;
using PlacesQuerying.Services.Clients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.ActivityResults
{
    public class YelpActivityResultService : IActivityResultService<YelpActivityResult, YelpActivityResultSummary>
    {
        private const int YELP_MAX_RESULTS = 1000;

        private const int YELP_RESULT_LIMIT = 10;

        private readonly IActivityTransformer<YelpBusinessResult> _yelpBusinessResultTransformer;

        private readonly IYelpClient _yelpClient;

        private readonly IYelpCategorySelector _yelpCategorySelector;

        private readonly IMapper _mapper;

        private readonly Random _random;

        public YelpActivityResultService(
            IActivityTransformer<YelpBusinessResult> yelpBusinessResultTransformer,
            IYelpClient yelpClient,
            IYelpCategorySelector yelpCategorySelector,
            IMapper mapper)
        {
            _yelpBusinessResultTransformer = yelpBusinessResultTransformer;
            _yelpClient = yelpClient;
            _yelpCategorySelector = yelpCategorySelector;
            _mapper = mapper;

            _random = new Random();
        }

        public async Task<YelpActivityResult> GetActivityResultAsync(ActivityFilter activityFilter, ActivityHistory activityHistory)
        {
            var lastActivityBatchDetailsWithYelpResults = activityHistory.RecentActivityBatchDetails
                .ContainingActivityResultSummaryOfType<YelpActivityResultSummary>()
                .FirstOrDefault();

            var mostRecentYelpActivityResultSummary = lastActivityBatchDetailsWithYelpResults?.GetActivityResultSummaryOfType<YelpActivityResultSummary>();

            var isTotalPossibleResultsKnown = TryGetTotalPossibleResults(out var totalPossibleResults);

            var matchingConsecutiveActivityBatchGroup = activityHistory.ConsecutiveActivityBatchGroups.FirstMatchingActivityFilterOrDefault(activityFilter);

            // When tags change the first results should always start at the beginning
            // This allows the total possible results to be determine for those tags in order to determine the maximum offset
            var offset = isTotalPossibleResultsKnown
                ? this.GetRandomOffset(totalPossibleResults.Value, matchingConsecutiveActivityBatchGroup)
                : 0;

            var searchCategories = activityFilter.Tags.Any()
                ? (await _yelpCategorySelector
                      .GetCategoriesAsync(activityFilter))
                      .Select(c => c.Alias)
                : (await _yelpCategorySelector
                      .GetRandomCategorySubsetAsync())
                      .Select(c => c.Alias);

            var request = new YelpBusinessSearchRequest
            {
                ApiKey = "KZeCdk0pezc_pKs9OopXWk5PD5cqi8onMSbSlgkMvB-xc1E_VKXICNzek0CXzmw5fT-nhw5uLxCdxywzWRgtzve7tNuQI_5B-jz0u_eOS-QDHCapP98pN9UtJVgnX3Yx",
                Version = "v3",
                Latitude = activityFilter.GeolocationCoordinates.Latitude,
                Longitude = activityFilter.GeolocationCoordinates.Longitude,
                Offset = offset,
                Limit = YELP_RESULT_LIMIT,
                Categories = searchCategories,
            };

            var response = await _yelpClient.BusinessSearchAsync(request);

            var activities = response.BusinessResults
                .Where(r => !r.IsPermanentlyClosed)
                .Select(r => _yelpBusinessResultTransformer.Transform(r))
                .ToList();

            return new YelpActivityResult
            {
                Activities = activities,
                TotalPossibleResults = response.TotalResults,
                SearchRange = new IntegerRange
                {
                    Start = offset,
                    End = offset + YELP_RESULT_LIMIT,
                },
            };

            bool TryGetTotalPossibleResults(out int? totalPossibleResults)
            {
                totalPossibleResults = null;

                if (IsTotalPossibleResultsUnknown())
                    return false;

                totalPossibleResults = mostRecentYelpActivityResultSummary.TotalPossibleResults;

                return true;
            }

            bool IsTotalPossibleResultsUnknown() =>
                mostRecentYelpActivityResultSummary == null || !lastActivityBatchDetailsWithYelpResults.ActivityFilter.HasSameTags(activityFilter);
        }

        public YelpActivityResultSummary MapActivityResultSummary(YelpActivityResult activityResult) =>
            _mapper.Map<YelpActivityResultSummary>(activityResult);

        private int GetRandomOffset(int totalPossibleResults, ConsecutiveActivityBatchGroup consecutiveActivityBatchGroup)
        {
            var maxOffset = Math.Min(YELP_MAX_RESULTS, totalPossibleResults) - YELP_RESULT_LIMIT;

            var maxOffsetSteps = (int)Math.Floor((double)maxOffset / YELP_RESULT_LIMIT);

            // The offset is randomized on a stepped scale of numbers divisible by the result limit
            // e.g. If 113 is the max offset and 10 is the result limit then the max offset steps will be 11
            // If the number 7 is randomly picked from between 0 and 11 it will then be multiplied by the result 
            // limit of 10 to get 70. Or if 3 was picked, the offset would be 30 etc.
            var offset = default(int);

            for (var randomOffsetAttempt = 0; randomOffsetAttempt < 5; randomOffsetAttempt++)
            {
                offset = _random.Next(0, maxOffsetSteps) * YELP_RESULT_LIMIT;

                if (consecutiveActivityBatchGroup != null && HasOffsetOccurredRecently(offset))
                    continue;

                break;
            }

            return offset;

            bool HasOffsetOccurredRecently(int offset) =>
                consecutiveActivityBatchGroup.ActivityBatchDetails
                    .Select(bd => bd.GetActivityResultSummaryOfType<YelpActivityResultSummary>())
                    .Any(s => s.SearchRange.IsBetween(offset));
        }
    }
}
