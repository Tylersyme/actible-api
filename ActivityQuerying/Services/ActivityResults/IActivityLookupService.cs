﻿using ActivityQuerying.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.ActivityResults
{
    public interface IActivityLookupService
    {
        Task<Activity> LookupActivityByIdentifierAsync(string identifier);
    }
}
