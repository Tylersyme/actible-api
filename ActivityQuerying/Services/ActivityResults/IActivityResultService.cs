﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Models.Yelp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.ActivityResults
{
    public interface IActivityResultService<TResult, TDetails> 
        where TResult : ActivityResult 
        where TDetails : ActivityResultSummary
    {
        Task<TResult> GetActivityResultAsync(ActivityFilter activityFilter, ActivityHistory activityHistory);

        TDetails MapActivityResultSummary(TResult activityResult);
    }
}
