﻿using ActivityQuerying.Models;
using ActivityQuerying.Services.Transformation;
using PlacesQuerying.Requests.Yelp.Search;
using PlacesQuerying.Responses.Yelp.Search;
using PlacesQuerying.Services.Clients;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services.ActivityResults
{
    public class YelpActivityLookupService : IActivityLookupService
    {
        private readonly IYelpClient _yelpClient;
        private readonly IActivityTransformer<YelpBusinessResult> _yelpActivityTransformer;

        public YelpActivityLookupService(
            IYelpClient yelpClient,
            IActivityTransformer<YelpBusinessResult> yelpActivityTransformer)
        {
            _yelpClient = yelpClient;
            _yelpActivityTransformer = yelpActivityTransformer;
        }

        public async Task<Activity> LookupActivityByIdentifierAsync(string identifier)
        {
            var result = await _yelpClient.BusinessDetailsAsync(new YelpBusinessDetailsRequest
            {
                ApiKey = "KZeCdk0pezc_pKs9OopXWk5PD5cqi8onMSbSlgkMvB-xc1E_VKXICNzek0CXzmw5fT-nhw5uLxCdxywzWRgtzve7tNuQI_5B-jz0u_eOS-QDHCapP98pN9UtJVgnX3Yx",
                Version = "v3",
                BusinessId = identifier,
            });

            var activity = _yelpActivityTransformer.Transform(result);

            return activity;
        }
    }
}
