﻿using ActivityQuerying.Models;
using ActivityQuerying.Filtering;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ActivityQuerying.Services
{
    public interface IActivityOrchestrator
    {
        Task<ActivityBatch> GetNextActivityBatchAsync(Guid userId, ActivityFilter activityFilter, ActivityHistory activityHistory);
    }
}
