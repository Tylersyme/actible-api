﻿using ActivityQuerying.Models;
using ActivityQuerying.Models.Yelp;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.MappingProfiles
{
    public class ActivityResultSummaryProfile : Profile
    {
        public ActivityResultSummaryProfile()
        {
            base.CreateMap<ActivityResult, ActivityResultSummary>();
            base.CreateMap<YelpActivityResult, YelpActivityResultSummary>();
            base.CreateMap<YelpActivityResultSummary, YelpActivityResult>();
        }
    }
}
