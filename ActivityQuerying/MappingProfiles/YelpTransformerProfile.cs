﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models.Components;
using AutoMapper;
using PlacesQuerying.Responses.Yelp.Search.Parts;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.MappingProfiles
{
    public class YelpTransformerProfile: Profile
    {
        public YelpTransformerProfile()
        {
            base.CreateMap<Coordinates, GeolocationCoordinates>();

            base.CreateMap<Location, LocationComponent>();
        }
    }
}
