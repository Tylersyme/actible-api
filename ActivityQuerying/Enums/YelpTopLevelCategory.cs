﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Enums
{
    public class YelpTopLevelCategory
    {
        public static readonly List<YelpTopLevelCategory> ALL = new List<YelpTopLevelCategory>();

        public static readonly YelpTopLevelCategory ActiveLife = new YelpTopLevelCategory("active");
        public static readonly YelpTopLevelCategory ArtsAndEntertainment = new YelpTopLevelCategory("arts");
        public static readonly YelpTopLevelCategory Automotive = new YelpTopLevelCategory("auto");
        public static readonly YelpTopLevelCategory BeautyAndSpas = new YelpTopLevelCategory("beautysvc");
        public static readonly YelpTopLevelCategory Bicycles = new YelpTopLevelCategory("bicycles");
        public static readonly YelpTopLevelCategory Education = new YelpTopLevelCategory("education");
        public static readonly YelpTopLevelCategory EventPlanningAndServices = new YelpTopLevelCategory("eventservices");
        public static readonly YelpTopLevelCategory FinancialServices = new YelpTopLevelCategory("financialservices");
        public static readonly YelpTopLevelCategory Food = new YelpTopLevelCategory("food");
        public static readonly YelpTopLevelCategory HealthAndMedical = new YelpTopLevelCategory("health");
        public static readonly YelpTopLevelCategory HomeServices = new YelpTopLevelCategory("homeservices");
        public static readonly YelpTopLevelCategory HotelsAndTravel = new YelpTopLevelCategory("hotelstravel");
        public static readonly YelpTopLevelCategory LocalFlavor = new YelpTopLevelCategory("localflavor");
        public static readonly YelpTopLevelCategory LocalServices = new YelpTopLevelCategory("localservices");
        public static readonly YelpTopLevelCategory MassMedia = new YelpTopLevelCategory("massmedia");
        public static readonly YelpTopLevelCategory Nightlife = new YelpTopLevelCategory("nightlife");
        public static readonly YelpTopLevelCategory Pets = new YelpTopLevelCategory("pets");
        public static readonly YelpTopLevelCategory ProfessionalServices = new YelpTopLevelCategory("professional");
        public static readonly YelpTopLevelCategory PublicServicesAndGovernment = new YelpTopLevelCategory("publicservicesgovt");
        public static readonly YelpTopLevelCategory RealEstate = new YelpTopLevelCategory("realestate");
        public static readonly YelpTopLevelCategory ReligiousOrganizations = new YelpTopLevelCategory("religiousorgs");
        public static readonly YelpTopLevelCategory Restaurants = new YelpTopLevelCategory("restaurants");
        public static readonly YelpTopLevelCategory Shopping = new YelpTopLevelCategory("shopping");

        public string Alias { get; set; }

        private YelpTopLevelCategory(string alias)
        {
            this.Alias = alias;

            ALL.Add(this);
        }
    }
}
