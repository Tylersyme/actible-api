﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Enums
{
    public enum ComponentType
    {
        PlaceInfo,
        PlaceReviews,
        LazyPlaceReviews,
        Images,
        Location,
    }
}
