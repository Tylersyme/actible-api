﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Enums
{
    public enum ActivityVerbType
    {
        Eat,
        Hike,
        Shop,
    }
}
