﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Enums
{
    public enum ActivityNounType
    {
        Home, // Work out at
        Garden, // Plant a
        BoardGame, // Play a

        AmusementPark,
        Bar,
        Beach, // Visit the, Get a tan at
        Bowling,
        CafePub, // Have a drink at
        CampingGround, // Camp at
        Casino, // Play craps at, have a drink at, play the slots at
        ComedyClub,
        Cinema, // Watch a movie at
        WildlifePark,
        SportsCourt,
        Park,
        PanoramicView,
        PlaceOfWorship,
        RecreationCenter, // 
        Store, // Shop for X at
        Convention,
        GolfCourse,
        IceSkating,
        TouristAttraction,
        Theater,
        Library, // Visit the, check out X at
        Club, // Hit the
        Trail, // Hike at
        Restaurant, // Eat at, try X at, take X on a date at
        Spa,
        SportsCenter,
        Stadium,
        SwimmingPool,
        Outdoors,
        Winery, // 
        LeisureCenter,
        Museum, // Visit the
        Zoo, // Visit the
    }
}
