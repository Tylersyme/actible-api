﻿using ActivityQuerying.Services;
using ActivityQuerying.Services.Builders;
using ActivityQuerying.Services.Cache;
using ActivityQuerying.Services.Mapping;
using ActivityQuerying.Services.Mapping.Noun;
using ActivityQuerying.Services.Transformation;
using ActivityQuerying.Services.ActivityResults;
using Autofac;
using PlacesQuerying.Responses.TomTom.Search;
using PlacesQuerying.Responses.Yelp.Search;
using System;
using System.Collections.Generic;
using System.Text;
using ActivityQuerying.Models.Yelp;

namespace ActivityQuerying
{
    public class ActivityQueryingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ActivityQueryService>()
                .As<IActivityQueryService>();

            builder
                .RegisterType<ActivityOrchestrator>()
                .As<IActivityOrchestrator>();

            builder
                .RegisterType<TomTomNearbyResponseTransformer>()
                .As<IActivityTransformer<TomTomSearchResult>>();

            builder
                .RegisterType<TomTomClassificationNounMappingService>()
                .As<ITomTomClassificationNounMappingService>();

            builder
                .RegisterType<ActivityDetailsService>()
                .As<IActivityDetailsService>();

            builder
                .RegisterType<YelpAllCategoriesMappingService>()
                .As<IYelpAllCategoriesMappingService>();

            builder
                .RegisterType<YelpCategoryCache>()
                .SingleInstance();

            builder
                .RegisterType<YelpBusinessResultTransformer>()
                .As<IActivityTransformer<YelpBusinessResult>>();

            builder
                .RegisterType<YelpCategorySelector>()
                .As<IYelpCategorySelector>();

            builder
                .RegisterType<YelpCategorySubsetBuilder>()
                .As<IYelpCategorySubsetBuilder>();

            builder
                .RegisterType<ActivityTagYelpCategoryMapper>()
                .As<IActivityTagYelpCategoryMapper>()
                .SingleInstance();

            builder
                .RegisterType<YelpActivityResultService>()
                .As<IActivityResultService<YelpActivityResult, YelpActivityResultSummary>>();

            builder
                .RegisterType<YelpActivityLookupService>()
                .As<IActivityLookupService>();
        }
    }
}
