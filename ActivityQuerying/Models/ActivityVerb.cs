﻿using ActivityQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models
{
    public class ActivityVerb
    {
        public ActivityVerbType Type { get; set; }
    }
}
