﻿using ActivityQuerying.Filtering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models
{
    public class ConsecutiveActivityBatchGroup
    {
        public ActivityFilter ActivityFilter { get; set; }

        public List<ActivityBatchDetails> ActivityBatchDetails { get; set; } = new List<ActivityBatchDetails>();
    }
}
