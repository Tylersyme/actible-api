﻿using ActivityQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Components
{
    public class LazyPlaceReviewsComponent : Component
    {
        public override ComponentType Type => ComponentType.LazyPlaceReviews;
    }
}
