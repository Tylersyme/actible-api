﻿using ActivityQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Components
{
    public class ImagesComponent : Component
    {
        public override ComponentType Type => ComponentType.Images;

        public List<string> ImageUrls { get; set; } = new List<string>();
    }
}
