﻿using ActivityQuerying.Enums;
using ActivityQuerying.Filtering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Components
{
    public class PlaceInfoComponent : Component
    {
        public override ComponentType Type => ComponentType.PlaceInfo;

        public string Name { get; set; }

        public string PhoneNumber { get; set; }

        public string WebsiteUrl { get; set; }

        public string PriceLevelTitle { get; set; }

        public IEnumerable<string> Categories { get; set; } = new List<string>();

        public decimal Distance { get; set; }

        public GeolocationCoordinates Coordinates { get; set; }
    }
}
