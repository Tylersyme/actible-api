﻿using ActivityQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Components
{
    public class PlaceReviewsComponent : Component
    {
        public override ComponentType Type => ComponentType.PlaceReviews;

        /// <summary>
        /// The source of the reviews (e.g. Yelp).
        /// </summary>
        public PlaceReviewSource PlaceReviewSource { get; set; }

        /// <summary>
        /// The overall rating of the place.
        /// </summary>
        public float Rating { get; set; }

        /// <summary>
        /// The total reviews of the place from the review source.
        /// </summary>
        public int ReviewCount { get; set; }

        /// <summary>
        /// The url leading to the place reviews at the review source.
        /// </summary>
        public string ReviewSourceUrl { get; set; }
    }
}
