﻿using ActivityQuerying.Enums;
using ActivityQuerying.Services.Converters;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace ActivityQuerying.Models.Components
{
    [JsonConverter(typeof(AsRuntimeTypeConverter<Component>))]
    public abstract class Component
    {
        public abstract ComponentType Type { get; }
    }
}
