﻿using ActivityQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models
{
    public class ActivityNoun
    {
        public ActivityNounType Type { get; set; }

        public IEnumerable<string> Adjectives { get; set; } = new List<string>();
    }
}
