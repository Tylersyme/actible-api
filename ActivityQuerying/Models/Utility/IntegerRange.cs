﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Utility
{
    public class IntegerRange : ValueRange<int>
    {
        public override bool IsBetween(int value) =>
            base.Start <= value && base.End >= value;
    }
}
