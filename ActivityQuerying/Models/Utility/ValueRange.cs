﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Utility
{
    public abstract class ValueRange<TValue>
    {
        public TValue Start { get; set; }

        public TValue End { get; set; }

        public abstract bool IsBetween(TValue value);
    }
}
