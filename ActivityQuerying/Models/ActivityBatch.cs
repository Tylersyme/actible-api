﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Models
{
    public class ActivityBatch
    {
        public List<ActivityResult> ActivityResults { get; set; } = new List<ActivityResult>();

        public List<ActivityResultSummary> ActivityResultSummary { get; set; } = new List<ActivityResultSummary>();

        public List<Activity> Activities =>
            this.ActivityResults
                .SelectMany(ab => ab.Activities)
                .ToList();
    }
}
