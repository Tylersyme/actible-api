﻿using ActivityQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models
{
    public abstract class ActivityResult
    {
        public abstract ActivityBatchType ActivityBatchType { get; }

        public List<Activity> Activities { get; set; } = new List<Activity>();
    }
}
