﻿using ActivityQuerying.Filtering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models
{
    public class ActivityBatchDetails
    {
        /// <summary>
        /// Gets or sets the activity filter which was used to produce the activity batch.
        /// </summary>
        public ActivityFilter ActivityFilter { get; set; }

        public List<ActivityResultSummary> ActivityResultSummaries { get; set; } = new List<ActivityResultSummary>();
    }
}
