﻿using ActivityQuerying.Enums;
using ActivityQuerying.Models.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Yelp
{
    public class YelpActivityResult : ActivityResult
    {
        public override ActivityBatchType ActivityBatchType =>
            ActivityBatchType.Yelp;

        public int TotalPossibleResults { get; set; }

        /// <summary>
        /// Gets or sets the result range used to get the results.
        /// </summary>
        public IntegerRange SearchRange { get; set; }
    }
}
