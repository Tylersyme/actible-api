﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Yelp
{
    public class YelpCategory
    {
        public string Alias { get; set; }

        public string Title { get; set; }

        public YelpCategory Parent { get; set; }

        public List<YelpCategory> ChildCategories { get; set; } = new List<YelpCategory>();

        public List<string> CountryWhitelist { get; set; }

        public List<string> CountryBlacklist { get; set; }
    }
}
