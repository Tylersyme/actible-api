﻿using ActivityQuerying.Enums;
using ActivityQuerying.Models.Utility;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models.Yelp
{
    public class YelpActivityResultSummary : ActivityResultSummary
    {
        public override ActivityResultType ActivityResultType =>
            ActivityResultType.Yelp;

        public int TotalPossibleResults { get; set; }

        /// <summary>
        /// Gets or sets the result range used to get the results.
        /// </summary>
        public IntegerRange SearchRange { get; set; }
    }
}
