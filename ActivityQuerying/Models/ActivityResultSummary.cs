﻿using ActivityQuerying.Enums;
using ActivityQuerying.Filtering;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Models
{
    public abstract class ActivityResultSummary
    {
        public abstract ActivityResultType ActivityResultType { get; }
    }
}
