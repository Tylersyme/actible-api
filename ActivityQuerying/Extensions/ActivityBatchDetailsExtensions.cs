﻿using ActivityQuerying.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Extensions
{
    public static class ActivityBatchDetailsExtensions
    {
        public static IEnumerable<ActivityBatchDetails> ContainingActivityResultSummaryOfType<TResult>(this IEnumerable<ActivityBatchDetails> activityBatchDetails)
            where TResult : ActivityResultSummary =>
            activityBatchDetails.Where(abd => abd.ActivityResultSummaries.OfType<TResult>().Any());

        public static TResult GetActivityResultSummaryOfType<TResult>(this ActivityBatchDetails activityBatchDetails)
            where TResult : ActivityResultSummary =>
            activityBatchDetails.ActivityResultSummaries
                .OfType<TResult>()
                .FirstOrDefault();
    }
}
