﻿using ActivityQuerying.Models.Yelp;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Extensions
{
    public static class YelpCategoryExtensions
    {
        public static YelpCategory GetTopmostParent(this YelpCategory yelpCategory)
        {
            if (yelpCategory.Parent == null)
                return yelpCategory;

            return yelpCategory.Parent.GetTopmostParent();
        }
    }
}
