﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Extensions
{
    public static class ConsecutiveActivityBatchGroupExtensions
    {
        public static ConsecutiveActivityBatchGroup FirstMatchingActivityFilterOrDefault(
            this IEnumerable<ConsecutiveActivityBatchGroup> consecutiveActivityBatchGroups, 
            ActivityFilter activityFilter)
        {
            return consecutiveActivityBatchGroups.FirstOrDefault(bg => bg.ActivityFilter.IsEquivalent(activityFilter));
        }
    }
}
