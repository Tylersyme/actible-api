﻿using ActivityQuerying.Filtering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Extensions
{
    public static class ActivityFilterExtensions
    {
        public static bool HasSameTags(this ActivityFilter activityFilter, ActivityFilter other)
        {
            if (activityFilter.Tags.Count() != other.Tags.Count())
                return false;

            return !activityFilter.Tags.Any(t => !other.Tags.Any(ot => ot.Name == t.Name));
        }

        public static bool IsEquivalent(this ActivityFilter activityFilter, ActivityFilter other) =>
            activityFilter.HasSameTags(other);
    }
}
