﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Filtering
{
    public class GeolocationCoordinates
    {
        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
