﻿using ActivityQuerying.Enums;
using ActivityQuerying.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Filtering
{
    public class ActivityHistory
    {
        public IEnumerable<ActivityBatchDetails> RecentActivityBatchDetails { get; set; } = new List<ActivityBatchDetails>();

        public IEnumerable<ConsecutiveActivityBatchGroup> ConsecutiveActivityBatchGroups { get; set; } = new List<ConsecutiveActivityBatchGroup>();

        public List<ActivityFilter> RecentActivityFilters =>
            this.RecentActivityBatchDetails
                .Select(abd => abd.ActivityFilter)
                .ToList();

        public ActivityBatchDetails MostRecentActivityBatchDetails =>
            this.RecentActivityBatchDetails.FirstOrDefault();

        public ActivityFilter MostRecentActivityFilter =>
            this.MostRecentActivityBatchDetails?.ActivityFilter;
    }
}
