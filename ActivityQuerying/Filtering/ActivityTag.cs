﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace ActivityQuerying.Filtering
{
    public class ActivityTag
    {
        public static readonly List<ActivityTag> ALL = new List<ActivityTag>();

        public static readonly ActivityTag Active = new ActivityTag("active");
        public static readonly ActivityTag Food = new ActivityTag("food");
        public static readonly ActivityTag Show = new ActivityTag("show");
        public static readonly ActivityTag WalkAndView = new ActivityTag("walk_and_view");
        public static readonly ActivityTag Relaxing = new ActivityTag("relaxing");
        public static readonly ActivityTag Beauty = new ActivityTag("beauty");

        public string Name { get; set; }

        private ActivityTag(string name)
        {
            this.Name = name;

            ALL.Add(this);
        }

        public static ActivityTag GetByName(string name) =>
            ALL.First(t => t.Name == name);
    }
}
