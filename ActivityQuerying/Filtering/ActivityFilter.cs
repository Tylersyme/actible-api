﻿using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Filtering
{
    public class ActivityFilter
    {
        public IEnumerable<ActivityTag> Tags { get; set; } = new List<ActivityTag>();

        /// <summary>
        /// Gets or sets the coordinates to bias the results to.
        /// </summary>
        public GeolocationCoordinates GeolocationCoordinates { get; set; }
    }
}
