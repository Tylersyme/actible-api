﻿using ActivityQuerying.Enums;
using Entities.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace ActivityQuerying.Filtering
{
    public class ActivityDetailsFilter
    {
        public ActivitySourceType ActivitySourceType { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

        public string City { get; set; }
        
        public string State { get; set; }

        public string Country { get; set; }

        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
