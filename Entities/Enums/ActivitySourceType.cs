﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Enums
{
    public enum ActivitySourceType
    {
        Database,
        TomTom,
        Yelp,
    }
}
