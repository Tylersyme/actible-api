﻿using Entities.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Activity
{
    public class ActivitySource
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public ActivitySourceType ActivitySourceType { get; set; }

        public string ActivityIdentifier { get; set; }

        public virtual ICollection<UserViewedActivitySourceMap> UserViewedActivitySourceMaps { get; set; } = new List<UserViewedActivitySourceMap>();

        public virtual ICollection<UserSavedActivitySourceMap> UserSavedActivitySourceMaps { get; set; } = new List<UserSavedActivitySourceMap>();
    }
}
