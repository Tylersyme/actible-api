﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Activity
{
    public class UserViewedActivitySourceMap
    {
        public Guid ActivitySourceId { get; set; }

        public virtual ActivitySource ActivitySource { get; set; }

        public Guid UserId { get; set; }

        public DateTime LastViewed { get; set; }

        public int TotalTimesViewed { get; set; }
    }
}
