﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Activity
{
    public class UserSavedActivitySourceMap
    {
        public Guid ActivitySourceId { get; set; }

        public virtual ActivitySource ActivitySource { get; set; }

        public Guid UserId { get; set; }

        public virtual ICollection<SavedActivityDetails> SavedActivityDetails { get; set; } = new List<SavedActivityDetails>();
    }
}
