﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Activity
{
    public class Activity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        [Required]
        public string Title { get; set; }

        public virtual ICollection<ActivityGenreMap> ActivityGenreMaps { get; set; } = new List<ActivityGenreMap>();

        public virtual ICollection<ActivityTagMap> ActivityTagMaps { get; set; } = new List<ActivityTagMap>();
    }
}
