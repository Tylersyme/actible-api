﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Entities.Activity
{
    public class SavedActivityDetails
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        public Guid UserId { get; set; }

        public Guid ActivitySourceId { get; set; }

        [Required]
        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public string Instructions { get; set; }

        public virtual ActivitySource ActivitySource { get; set; }

        public virtual UserSavedActivitySourceMap UserSavedActivitySourceMap { get; set; }
    }
}
