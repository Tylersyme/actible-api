﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Activity
{
    public class ActivityGenreMap
    {
        public Guid ActivityId { get; set; }

        public virtual Activity Activity { get; set; }

        public Guid GenreId { get; set; }

        public virtual Genre Genre { get; set; }

        public bool IsPrimary { get; set; }
    }
}
