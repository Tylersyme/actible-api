﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Entities.Activity
{
    public class ActivityTagMap
    {
        public Guid ActivityId { get; set; }

        public virtual Activity Activity { get; set; }

        public Guid TagId { get; set; }

        public virtual Tag Tag { get; set; }

        public int Count { get; set; }
    }
}
