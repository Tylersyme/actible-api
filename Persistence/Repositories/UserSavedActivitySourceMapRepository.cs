﻿using Entities.Activity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class UserSavedActivitySourceMapRepository : IUserSavedActivitySourceMapRepository
    {
        private readonly AudlyContext _audlyContext;

        public UserSavedActivitySourceMapRepository(AudlyContext audlyContext)
        {
            _audlyContext = audlyContext;
        }

        public async Task<UserSavedActivitySourceMap> GetSavedActivitySourceMapAsync(Guid userId, string activityIdentifier) =>
            await _audlyContext
                .Set<UserSavedActivitySourceMap>()
                .FirstOrDefaultAsync(usm => usm.UserId == userId && usm.ActivitySource.ActivityIdentifier == activityIdentifier);

        public async Task DeleteSavedActivitySourceMapAsync(Guid userId, string activityIdentifier)
        {
            var savedActivitySourceMap = await this.GetSavedActivitySourceMapAsync(userId, activityIdentifier);

             _audlyContext
                .Set<UserSavedActivitySourceMap>()
                .Remove(savedActivitySourceMap);

            await _audlyContext.SaveChangesAsync();
        }
    }
}
