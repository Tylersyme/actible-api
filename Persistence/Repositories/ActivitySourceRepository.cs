﻿using Entities.Activity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class ActivitySourceRepository : IActivitySourceRepository
    {
        private readonly AudlyContext _audlyContext;

        public ActivitySourceRepository(AudlyContext audlyContext)
        {
            _audlyContext = audlyContext;
        }

        public async Task AddUniqueAsync(IEnumerable<ActivitySource> activitySources)
        {
            foreach (var activitySource in activitySources)
            {
                var exists = await _audlyContext
                    .Set<ActivitySource>()
                    .AnyAsync(a => a.ActivityIdentifier == activitySource.ActivityIdentifier);

                if (exists)
                    continue;

                _audlyContext
                    .Set<ActivitySource>()
                    .Add(activitySource);
            }

            await _audlyContext.SaveChangesAsync();
        }
    }
}
