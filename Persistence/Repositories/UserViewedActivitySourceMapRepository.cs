﻿using Entities.Activity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class UserViewedActivitySourceMapRepository : IUserViewedActivitySourceMapRepository
    {
        private readonly AudlyContext _audlyContext;

        public UserViewedActivitySourceMapRepository(AudlyContext audlyContext)
        {
            _audlyContext = audlyContext;
        }

        public async Task<IEnumerable<ActivitySource>> GetMostRecentActivitySourcesAsync(Guid userId, int totalResults) =>
            await _audlyContext
                .Set<UserViewedActivitySourceMap>()
                .Where(uas => uas.UserId == userId)
                .OrderByDescending(uas => uas.LastViewed)
                .Take(totalResults)
                .Select(uas => uas.ActivitySource)
                .ToListAsync();

        public async Task AddOrUpdateFromActivityIdentifiersAsync(Guid userId, IEnumerable<string> activityIdentifiers)
        {
            var activitySources = await _audlyContext
                .Set<ActivitySource>()
                .Where(a => activityIdentifiers.Contains(a.ActivityIdentifier))
                .ToListAsync();

            var userViewedActivitySourceMaps = await _audlyContext
                .Set<UserViewedActivitySourceMap>()
                .Where(uas => uas.UserId == userId && activityIdentifiers.Contains(uas.ActivitySource.ActivityIdentifier))
                .ToListAsync();

            foreach (var activitySource in activitySources)
            {
                var existingUserViewedActivitySourceMap = userViewedActivitySourceMaps.FirstOrDefault(uas => uas.ActivitySourceId == activitySource.Id);

                // Update the map if it already exists
                if (existingUserViewedActivitySourceMap != null)
                {
                    this.UpdateUserViewActivitySource(existingUserViewedActivitySourceMap);

                    continue;
                }

                var userViewedActivitySourceMap = new UserViewedActivitySourceMap
                {
                    UserId = userId,
                    ActivitySourceId = activitySource.Id,
                    LastViewed = DateTime.Now,
                    TotalTimesViewed = 1,
                };

                _audlyContext
                    .Set<UserViewedActivitySourceMap>()
                    .Add(userViewedActivitySourceMap);
            }

            await _audlyContext.SaveChangesAsync();
        }

        public async Task<ActivitySource> GetActivitySourceByUserAndIdentifierAsync(Guid userId, string activityIdentifier) =>
            await _audlyContext
                .Set<UserViewedActivitySourceMap>()
                .Include(uas => uas.ActivitySource)
                .Where(uas => uas.UserId == userId)
                .Select(uas => uas.ActivitySource)
                .FirstOrDefaultAsync(a => a.ActivityIdentifier == activityIdentifier);

        private void UpdateUserViewActivitySource(UserViewedActivitySourceMap userViewedActivitySourceMap)
        {
            ++userViewedActivitySourceMap.TotalTimesViewed;

            userViewedActivitySourceMap.LastViewed = DateTime.Now;
        }
    }
}
