﻿using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public interface IActivityGenreRepository
    {
        Task<List<Genre>> GetAllAsync();
    }
}
