﻿using Entities.Activity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class ActivityRepository : IActivityRepository
    {
        private readonly AudlyContext _audlyContext;

        public ActivityRepository(AudlyContext audlyContext)
        {
            _audlyContext = audlyContext;
        }

        public async Task<List<Activity>> GetActivitiesContainingAnyTagAsync(IEnumerable<string> tagNames) =>
            await _audlyContext
                .Set<Activity>()
                .Include(a => a.ActivityGenreMaps)
                    .ThenInclude(agm => agm.Genre)
                .Include(a => a.ActivityTagMaps)
                    .ThenInclude(atm => atm.Tag)
                .Where(a => a.ActivityTagMaps.Any(atm => tagNames.Contains(atm.Tag.Name)))
                .ToListAsync();
    }
}
