﻿using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public interface IUserSavedActivitySourceMapRepository
    {
        Task<UserSavedActivitySourceMap> GetSavedActivitySourceMapAsync(Guid userId, string activityIdentifier);

        Task DeleteSavedActivitySourceMapAsync(Guid userId, string activityIdentifier);
    }
}
