﻿using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public interface ITagRepository
    {
        Task<Tag> GetTagByNameAsync(string name);

        Task<List<Tag>> GetTagsByNamesAsync(IEnumerable<string> names);
    }
}
