﻿using Entities.Activity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class ActivityGenreRepository : IActivityGenreRepository
    {
        private readonly AudlyContext _audlyContext;

        public ActivityGenreRepository(AudlyContext audlyContext)
        {
            _audlyContext = audlyContext;
        }

        public async Task<List<Genre>> GetAllAsync() =>
            await _audlyContext
                .Set<Genre>()
                .ToListAsync();
    }
}
