﻿using Entities.Activity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public class TagRepository : ITagRepository
    {
        private readonly AudlyContext _audlyContext;

        public TagRepository(AudlyContext audlyContext)
        {
            _audlyContext = audlyContext;
        }

        public async Task<Tag> GetTagByNameAsync(string name) =>
            await _audlyContext
                .Set<Tag>()
                .FirstAsync(t => t.Name == name);

        public async Task<List<Tag>> GetTagsByNamesAsync(IEnumerable<string> names) =>
            await _audlyContext
                .Set<Tag>()
                .Where(t => names.Any(n => n == t.Name))
                .ToListAsync();
    }
}
