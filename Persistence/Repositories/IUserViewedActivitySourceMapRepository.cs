﻿using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public interface IUserViewedActivitySourceMapRepository
    {
        Task AddOrUpdateFromActivityIdentifiersAsync(Guid userId, IEnumerable<string> activityIdentifiers);

        Task<IEnumerable<ActivitySource>> GetMostRecentActivitySourcesAsync(Guid userId, int totalResults);

        Task<ActivitySource> GetActivitySourceByUserAndIdentifierAsync(Guid userId, string activityIdentifier);
    }
}
