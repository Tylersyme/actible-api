﻿using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Persistence.Repositories
{
    public interface IActivityRepository
    {
        Task<List<Activity>> GetActivitiesContainingAnyTagAsync(IEnumerable<string> tagNames);
    }
}
