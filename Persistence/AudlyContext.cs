﻿using Entities.Activity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence
{
    public class AudlyContext : DbContext
    {
        public AudlyContext(DbContextOptions<AudlyContext> options)
            : base(options)
        {
        }

        public DbSet<Activity> Activities { get; set; }

        public DbSet<Genre> ActivityGenres { get; set; }

        public DbSet<ActivityGenreMap> ActivityGenreMaps { get; set; }

        public DbSet<ActivitySource> ActivitySources { get; set; }

        public DbSet<UserViewedActivitySourceMap> UserViewedActivitySourceMaps { get; set; }

        public DbSet<UserSavedActivitySourceMap> UserSavedActivitySourceMaps { get; set; }

        public DbSet<SavedActivityDetails> SavedActivityDetails { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            #region ActivityGenreMap

            modelBuilder
                .Entity<ActivityGenreMap>()
                .HasKey(agm => new { agm.ActivityId, agm.GenreId });

            modelBuilder
                .Entity<ActivityGenreMap>()
                .HasOne(agm => agm.Activity)
                .WithMany(a => a.ActivityGenreMaps)
                .HasForeignKey(agm => agm.ActivityId);

            modelBuilder
                .Entity<ActivityGenreMap>()
                .HasOne(agm => agm.Genre)
                .WithMany(g => g.ActivityGenreMaps)
                .HasForeignKey(agm => agm.GenreId);

            modelBuilder
                .Entity<ActivityGenreMap>()
                .Property(agm => agm.IsPrimary)
                .HasDefaultValue(false);

            #endregion

            #region ActivityTagMap

            modelBuilder
                .Entity<ActivityTagMap>()
                .HasKey(atm => new { atm.ActivityId, atm.TagId });

            modelBuilder
                .Entity<ActivityTagMap>()
                .HasOne(atm => atm.Activity)
                .WithMany(a => a.ActivityTagMaps)
                .HasForeignKey(atm => atm.ActivityId);

            modelBuilder
                .Entity<ActivityTagMap>()
                .HasOne(atm => atm.Tag)
                .WithMany(t => t.ActivityTagMaps)
                .HasForeignKey(atm => atm.TagId);

            #endregion

            #region UserViewedActivitySourceMap

            modelBuilder
                .Entity<UserViewedActivitySourceMap>()
                .HasKey(uam => new { uam.ActivitySourceId, uam.UserId });

            modelBuilder
                .Entity<UserViewedActivitySourceMap>()
                .HasOne(uam => uam.ActivitySource)
                .WithMany(s => s.UserViewedActivitySourceMaps)
                .HasForeignKey(uam => uam.ActivitySourceId);

            #endregion

            #region UserSavedActivitySourceMap

            modelBuilder
                .Entity<UserSavedActivitySourceMap>()
                .HasKey(usm => new { usm.ActivitySourceId, usm.UserId });

            modelBuilder
                .Entity<UserSavedActivitySourceMap>()
                .HasOne(usm => usm.ActivitySource)
                .WithMany(s => s.UserSavedActivitySourceMaps)
                .HasForeignKey(usm => usm.ActivitySourceId);

            #endregion

            #region

            modelBuilder
                .Entity<SavedActivityDetails>()
                .HasKey(s => s.Id);

            modelBuilder
                .Entity<SavedActivityDetails>()
                .HasOne(s => s.ActivitySource)
                .WithMany()
                .HasForeignKey(s => s.ActivitySourceId);

            modelBuilder
                .Entity<SavedActivityDetails>()
                .HasOne(s => s.UserSavedActivitySourceMap)
                .WithMany(usm => usm.SavedActivityDetails)
                .HasForeignKey(s => new { s.ActivitySourceId, s.UserId });

            #endregion

            #region ActivitySource

            modelBuilder
                .Entity<ActivitySource>()
                .HasIndex(a => a.ActivityIdentifier)
                .IsUnique();

            #endregion

            this.Seed(modelBuilder);
        }

        private void Seed(ModelBuilder modelBuilder)
        {
        }
    }
}
