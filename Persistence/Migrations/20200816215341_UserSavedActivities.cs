﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class UserSavedActivities : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "UserSavedActivitySourceMaps",
                columns: table => new
                {
                    ActivitySourceId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserSavedActivitySourceMaps", x => new { x.ActivitySourceId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserSavedActivitySourceMaps_ActivitySources_ActivitySourceId",
                        column: x => x.ActivitySourceId,
                        principalTable: "ActivitySources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserSavedActivitySourceMaps_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserSavedActivitySourceMaps_UserId",
                table: "UserSavedActivitySourceMaps",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "UserSavedActivitySourceMaps");
        }
    }
}
