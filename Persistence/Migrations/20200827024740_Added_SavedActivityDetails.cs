﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class Added_SavedActivityDetails : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "SavedActivityDetails",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    ActivitySourceId = table.Column<Guid>(nullable: false),
                    StartTime = table.Column<DateTime>(nullable: false),
                    EndTime = table.Column<DateTime>(nullable: false),
                    Instructions = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_SavedActivityDetails", x => x.Id);
                    table.ForeignKey(
                        name: "FK_SavedActivityDetails_ActivitySources_ActivitySourceId",
                        column: x => x.ActivitySourceId,
                        principalTable: "ActivitySources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SavedActivityDetails_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_SavedActivityDetails_UserSavedActivitySourceMaps_ActivitySou~",
                        columns: x => new { x.ActivitySourceId, x.UserId },
                        principalTable: "UserSavedActivitySourceMaps",
                        principalColumns: new[] { "ActivitySourceId", "UserId" },
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_SavedActivityDetails_UserId",
                table: "SavedActivityDetails",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedActivityDetails_ActivitySourceId_UserId",
                table: "SavedActivityDetails",
                columns: new[] { "ActivitySourceId", "UserId" });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "SavedActivityDetails");
        }
    }
}
