﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class Removed_Users : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_SavedActivityDetails_Users_UserId",
                table: "SavedActivityDetails");

            migrationBuilder.DropForeignKey(
                name: "FK_UserSavedActivitySourceMaps_Users_UserId",
                table: "UserSavedActivitySourceMaps");

            migrationBuilder.DropForeignKey(
                name: "FK_UserViewedActivitySourceMaps_Users_UserId",
                table: "UserViewedActivitySourceMaps");

            migrationBuilder.DropTable(
                name: "Users");

            migrationBuilder.DropIndex(
                name: "IX_UserViewedActivitySourceMaps_UserId",
                table: "UserViewedActivitySourceMaps");

            migrationBuilder.DropIndex(
                name: "IX_UserSavedActivitySourceMaps_UserId",
                table: "UserSavedActivitySourceMaps");

            migrationBuilder.DropIndex(
                name: "IX_SavedActivityDetails_UserId",
                table: "SavedActivityDetails");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "char(36)", nullable: false),
                    Email = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false),
                    HashedPassword = table.Column<byte[]>(type: "longblob", nullable: false),
                    Username = table.Column<string>(type: "longtext CHARACTER SET utf8mb4", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateIndex(
                name: "IX_UserViewedActivitySourceMaps_UserId",
                table: "UserViewedActivitySourceMaps",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_UserSavedActivitySourceMaps_UserId",
                table: "UserSavedActivitySourceMaps",
                column: "UserId");

            migrationBuilder.CreateIndex(
                name: "IX_SavedActivityDetails_UserId",
                table: "SavedActivityDetails",
                column: "UserId");

            migrationBuilder.AddForeignKey(
                name: "FK_SavedActivityDetails_Users_UserId",
                table: "SavedActivityDetails",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserSavedActivitySourceMaps_Users_UserId",
                table: "UserSavedActivitySourceMaps",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_UserViewedActivitySourceMaps_Users_UserId",
                table: "UserViewedActivitySourceMaps",
                column: "UserId",
                principalTable: "Users",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
