﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Persistence.Migrations
{
    public partial class MySqlInitial : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Activities",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Title = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Activities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivityGenres",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityGenres", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivitySources",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    ActivitySourceType = table.Column<int>(nullable: false),
                    ActivityIdentifier = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivitySources", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Users",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    Username = table.Column<string>(nullable: false),
                    Email = table.Column<string>(nullable: false),
                    HashedPassword = table.Column<byte[]>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Users", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ActivityGenreMaps",
                columns: table => new
                {
                    ActivityId = table.Column<Guid>(nullable: false),
                    GenreId = table.Column<Guid>(nullable: false),
                    IsPrimary = table.Column<bool>(nullable: false, defaultValue: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityGenreMaps", x => new { x.ActivityId, x.GenreId });
                    table.ForeignKey(
                        name: "FK_ActivityGenreMaps_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityGenreMaps_ActivityGenres_GenreId",
                        column: x => x.GenreId,
                        principalTable: "ActivityGenres",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "ActivityTagMap",
                columns: table => new
                {
                    ActivityId = table.Column<Guid>(nullable: false),
                    TagId = table.Column<Guid>(nullable: false),
                    Count = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ActivityTagMap", x => new { x.ActivityId, x.TagId });
                    table.ForeignKey(
                        name: "FK_ActivityTagMap_Activities_ActivityId",
                        column: x => x.ActivityId,
                        principalTable: "Activities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ActivityTagMap_Tag_TagId",
                        column: x => x.TagId,
                        principalTable: "Tag",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "UserViewedActivitySourceMaps",
                columns: table => new
                {
                    ActivitySourceId = table.Column<Guid>(nullable: false),
                    UserId = table.Column<Guid>(nullable: false),
                    LastViewed = table.Column<DateTime>(nullable: false),
                    TotalTimesViewed = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_UserViewedActivitySourceMaps", x => new { x.ActivitySourceId, x.UserId });
                    table.ForeignKey(
                        name: "FK_UserViewedActivitySourceMaps_ActivitySources_ActivitySourceId",
                        column: x => x.ActivitySourceId,
                        principalTable: "ActivitySources",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_UserViewedActivitySourceMaps_Users_UserId",
                        column: x => x.UserId,
                        principalTable: "Users",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ActivityGenreMaps_GenreId",
                table: "ActivityGenreMaps",
                column: "GenreId");

            migrationBuilder.CreateIndex(
                name: "IX_ActivitySources_ActivityIdentifier",
                table: "ActivitySources",
                column: "ActivityIdentifier",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_ActivityTagMap_TagId",
                table: "ActivityTagMap",
                column: "TagId");

            migrationBuilder.CreateIndex(
                name: "IX_UserViewedActivitySourceMaps_UserId",
                table: "UserViewedActivitySourceMaps",
                column: "UserId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ActivityGenreMaps");

            migrationBuilder.DropTable(
                name: "ActivityTagMap");

            migrationBuilder.DropTable(
                name: "UserViewedActivitySourceMaps");

            migrationBuilder.DropTable(
                name: "ActivityGenres");

            migrationBuilder.DropTable(
                name: "Activities");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "ActivitySources");

            migrationBuilder.DropTable(
                name: "Users");
        }
    }
}
