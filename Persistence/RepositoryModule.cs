﻿using Autofac;
using Entities.Activity;
using Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;

namespace Persistence
{
    public class RepositoryModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<ActivityRepository>()
                .As<IActivityRepository>();

            builder
                .RegisterType<ActivityGenreRepository>()
                .As<IActivityGenreRepository>();

            builder
                .RegisterType<TagRepository>()
                .As<ITagRepository>();

            builder
                .RegisterType<ActivitySourceRepository>()
                .As<IActivitySourceRepository>();

            builder
                .RegisterType<UserViewedActivitySourceMapRepository>()
                .As<IUserViewedActivitySourceMapRepository>();

            builder
                .RegisterType<UserSavedActivitySourceMapRepository>()
                .As<IUserSavedActivitySourceMapRepository>();
        }
    }
}
