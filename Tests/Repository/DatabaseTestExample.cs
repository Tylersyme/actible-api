﻿using AutoFixture;
using FluentAssertions;
using Microsoft.EntityFrameworkCore;
using Persistence;
using Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace Tests.Repository
{
    public class DatabaseTestExample
    {
        private Fixture _fixture;

        public DatabaseTestExample()
        {
            _fixture = new Fixture();

            var optionsBuilder = new DbContextOptionsBuilder<AudlyContext>();

            optionsBuilder.UseInMemoryDatabase("TestDatabase");

            var audlyContext = new AudlyContext(optionsBuilder.Options);
            audlyContext.Database.EnsureDeleted();
            audlyContext.Database.EnsureCreated();
        }
    }
}
