﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.AppSettings
{
    public class ConnectionStringSettingsModel
    {
        public string AudlyConnectionString { get; set; }
    }
}
