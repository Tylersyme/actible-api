﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.AppSettings
{
    public class ActivityCacheSettings
    {
        public const string SECTION_NAME = "ActivityCache";

        public int ExpirationSeconds { get; set; }

        public int MaxEntries { get; set; }
    }
}
