﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.AppSettings
{
    public class GoogleSettings
    {
        public const string SECTION_NAME = "Google";

        public string ApiKey { get; set; }
    }
}
