﻿using ActivityQuerying.Filtering;
using AudlyApi.RequestModels;
using AutoMapper;
using Org.BouncyCastle.Asn1.Mozilla;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.MappingProfiles
{
    public class ActivityFilterProfile : Profile
    {
        public ActivityFilterProfile()
        {
            base.CreateMap<ActivityQuerying.Filtering.GeolocationCoordinates, RequestModels.GeolocationCoordinates>();
            base.CreateMap<RequestModels.GeolocationCoordinates, ActivityQuerying.Filtering.GeolocationCoordinates>();
        }
    }
}
