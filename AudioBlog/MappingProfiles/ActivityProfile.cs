﻿using ActivityQuerying.Filtering;
using AudlyApi.Requests;
using AudlyApi.Responses;
using AutoMapper;
using Entities.Activity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.MappingProfiles
{
    public class ActivityProfile : Profile
    {
        public ActivityProfile()
        {
            base.CreateMap<Genre, GenreResponse>();

            base
                .CreateMap<Tag, TagResponse>()
                .ForMember(dest => dest.Count, opts => opts.MapFrom(src => src.ActivityTagMaps.First(atm => atm.TagId == src.Id).Count));

            base
                .CreateMap<ActivityRequest, ActivityFilter>()
                .ForMember(dest => dest.Tags, opts => opts.MapFrom(src => src.Tags.Select(t => ActivityTag.GetByName(t))));

            base.CreateMap<ActivityDetailsRequest, ActivityDetailsFilter>();
        }
    }
}
