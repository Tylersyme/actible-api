﻿using AudlyApi.RequestModels;
using AudlyApi.Requests;
using AudlyApi.Responses;
using AutoMapper;
using GoogleApi.Entities.Common;
using GoogleApi.Entities.Places.AutoComplete.Request;
using GoogleApi.Entities.Places.Common;
using GoogleApi.Entities.Places.Details.Request;
using GoogleApi.Entities.Places.Details.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.MappingProfiles
{
    public class PlaceAutocompleteProfile : Profile
    {
        public PlaceAutocompleteProfile()
        {
            base.CreateMap<PlaceAutocompleteRequest, PlacesAutoCompleteRequest>();

            base.CreateMap<PlaceAutocompleteDetailsRequest, PlacesDetailsRequest>();

            base
                .CreateMap<Prediction, PlaceAutocompleteResponse>()
                .ForMember(src => src.PredictionText, opts => opts.MapFrom(dest => dest.Description));

            base
                .CreateMap<DetailsResult, PlaceAutocompleteDetailsResponse>()
                .ForMember(src => src.GeolocationCoordinates, opts => opts.MapFrom(dest => dest.Geometry.Location));

            base.CreateMap<Location, GeolocationCoordinates>();
        }
    }
}
