﻿using AudlyApi.Requests;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.MappingProfiles
{
    public class SavedActivityProfile : Profile
    {
        public SavedActivityProfile()
        {
            base.CreateMap<Entities.Activity.SavedActivityDetails, AudlyApi.RequestModels.SavedActivityDetails>();
            base.CreateMap<AudlyApi.RequestModels.SavedActivityDetails, Entities.Activity.SavedActivityDetails>();
        }
    }
}
