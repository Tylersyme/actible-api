﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Responses
{
    public class GenreResponse
    {
        public string Name { get; set; }
    }
}
