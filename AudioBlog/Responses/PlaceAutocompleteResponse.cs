﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Responses
{
    public class PlaceAutocompleteResponse
    {
        public string PredictionText { get; set; }

        public string PlaceId { get; set; }
    }
}
