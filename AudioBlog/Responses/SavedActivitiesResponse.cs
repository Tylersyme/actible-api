﻿using ActivityQuerying.Models;
using AudlyApi.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Responses
{
    public class SavedActivitiesResponse
    {
        public IEnumerable<SavedActivity> SavedActivities { get; set; } = new List<SavedActivity>();
    }
}
