﻿using AudlyApi.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Responses
{
    public class PlaceAutocompleteDetailsResponse
    {
        public GeolocationCoordinates GeolocationCoordinates { get; set; }
    }
}
