﻿using ActivityQuerying.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Responses
{
    public class ActivityResponse
    {
        public Activity Activity { get; set; }
    }
}
