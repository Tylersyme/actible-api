﻿using ActivityQuerying.Models.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Responses
{
    public class ActivityDetailsResponse
    {
        public IEnumerable<Component> DetailComponents { get; set; } = new List<Component>();
    }
}
