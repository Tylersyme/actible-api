﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Responses
{
    public class TagResponse
    {
        public string Name { get; set; }

        public int Count { get; set; }
    }
}
