﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using AudlyApi.Services.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Cache
{
    public interface IActivityCacheManager
    {
        void PushActivityBatchDetails(Guid userId, ActivityBatchDetails activityBatchDetails);

        bool TryGetActivityCacheData(Guid userId, out ActivityCacheData activityCacheData);
    }
}
