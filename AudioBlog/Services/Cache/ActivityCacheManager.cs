﻿using ActivityQuerying.Extensions;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using AudlyApi.AppSettings;
using AudlyApi.Services.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Cache
{
    public class ActivityCacheManager : IActivityCacheManager
    {
        private const string ACTIVITY_DATA_PREFIX = "activity";

        private readonly IMemoryCache _memoryCache;

        private readonly ActivityCacheSettings _settings;

        public ActivityCacheManager(
            IMemoryCache memoryCache,
            IOptions<ActivityCacheSettings> options)
        {
            _memoryCache = memoryCache;

            _settings = options.Value;
        }

        /// <summary>
        /// Pushes the given activity batch into the cache for the given user.
        /// If the max total activity batches has been reached, the oldest will be popped to make room for the new one.
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="activityBatchDetails"></param>
        public void PushActivityBatchDetails(Guid userId, ActivityBatchDetails activityBatchDetails)
        {
            var activityCacheData = this.GetActivityCacheData(userId);

            activityCacheData.RecentActivityBatchDetails.Insert(0, activityBatchDetails);

            if (MaxEntriesExceeded())
                PopLeastRecentActivityBatch();

            this.UpdateConsecutiveActivityBatchDetails(activityCacheData.ConsecutiveActivityBatchGroups, activityBatchDetails);

            this.SetActivityCacheData(userId, activityCacheData);

            bool MaxEntriesExceeded() =>
                activityCacheData.RecentActivityBatchDetails.Count > _settings.MaxEntries;

            void PopLeastRecentActivityBatch() => 
                activityCacheData.RecentActivityBatchDetails.RemoveAt(activityCacheData.RecentActivityBatchDetails.Count - 1);
        }

        public bool TryGetActivityCacheData(Guid userId, out ActivityCacheData activityCacheData)
        {
            var cacheKey = this.GetCacheKey(userId);

            var found = _memoryCache.TryGetValue(cacheKey, out activityCacheData);

            return found;
        }

        /// <summary>
        /// Updates the consecutive activity batches with the given activity batch details.
        /// </summary>
        /// <param name="consecutiveActivityBatchGroups"></param>
        /// <param name="newBatchDetails"></param>
        private void UpdateConsecutiveActivityBatchDetails(List<ConsecutiveActivityBatchGroup> consecutiveActivityBatchGroups, ActivityBatchDetails newBatchDetails)
        {
            var matchingConsecutiveActivityBatch = consecutiveActivityBatchGroups.FirstMatchingActivityFilterOrDefault(newBatchDetails.ActivityFilter);

            if (matchingConsecutiveActivityBatch == null)
                consecutiveActivityBatchGroups.Add(new ConsecutiveActivityBatchGroup
                {
                    ActivityFilter = newBatchDetails.ActivityFilter,
                    ActivityBatchDetails = new List<ActivityBatchDetails> { newBatchDetails },
                });
            else
                matchingConsecutiveActivityBatch.ActivityBatchDetails.Add(newBatchDetails);

            EnsureMaxEntriesNotExceeded();

            void EnsureMaxEntriesNotExceeded()
            {
                if (consecutiveActivityBatchGroups.Count > _settings.MaxEntries)
                    consecutiveActivityBatchGroups.RemoveAt(consecutiveActivityBatchGroups.Count - 1);

                if (matchingConsecutiveActivityBatch != null && 
                    matchingConsecutiveActivityBatch.ActivityBatchDetails.Count > _settings.MaxEntries)
                    matchingConsecutiveActivityBatch.ActivityBatchDetails.RemoveAt(matchingConsecutiveActivityBatch.ActivityBatchDetails.Count - 1);
            }
        }

        private void SetActivityCacheData(Guid userId, ActivityCacheData activityCacheData)
        {
            var cacheKey = this.GetCacheKey(userId);

            _memoryCache.Set(cacheKey, activityCacheData);
        }

        private ActivityCacheData GetActivityCacheData(Guid userId)
        {
            var cacheKey = this.GetCacheKey(userId);

            var activityCacheData = _memoryCache.GetOrCreate(cacheKey, entry =>
            {
                entry.SlidingExpiration = TimeSpan.FromSeconds(_settings.ExpirationSeconds);

                return new ActivityCacheData();
            });

            return activityCacheData;
        }

        private string GetCacheKey(Guid userId) =>
            $"{ACTIVITY_DATA_PREFIX}.{userId}";
    }
}
