﻿using GoogleApi;
using GoogleApi.Entities.Common.Enums;
using GoogleApi.Entities.Places.AutoComplete.Request;
using GoogleApi.Entities.Places.Common;
using GoogleApi.Entities.Places.Details.Request;
using GoogleApi.Entities.Places.Details.Response;
using PlacesQuerying.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Clients
{
    public class GooglePlacesClient : IGooglePlacesClient
    {
        public async Task<IEnumerable<Prediction>> GetAutocompletePredictionsAsync(PlacesAutoCompleteRequest request)
        {
            var response = await GooglePlaces.AutoComplete.QueryAsync(request);

            if (response.Status != Status.Ok)
                throw new ApiRequestException();

            return response.Predictions;
        }

        public async Task<DetailsResult> GetDetailsAsync(PlacesDetailsRequest request)
        {
            var response = await GooglePlaces.Details.QueryAsync(request);

            if (response.Status != Status.Ok)
                throw new ApiRequestException();

            return response.Result;
        }
    }
}
