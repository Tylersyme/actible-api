﻿using GoogleApi.Entities.Places.AutoComplete.Request;
using GoogleApi.Entities.Places.Common;
using GoogleApi.Entities.Places.Details.Request;
using GoogleApi.Entities.Places.Details.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Clients
{
    public interface IGooglePlacesClient
    {
        Task<IEnumerable<Prediction>> GetAutocompletePredictionsAsync(PlacesAutoCompleteRequest request);

        Task<DetailsResult> GetDetailsAsync(PlacesDetailsRequest request);
    }
}
