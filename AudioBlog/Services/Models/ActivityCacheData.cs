﻿using ActivityQuerying.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Models
{
    public class ActivityCacheData
    {
        /// <summary>
        /// Gets or sets the activities batch details in chronological order.
        /// </summary>
        public List<ActivityBatchDetails> RecentActivityBatchDetails { get; set; } = new List<ActivityBatchDetails>();

        /// <summary>
        /// Gets or sets the activity result summaries which have consecutively shared the same activity filter.
        /// </summary>
        public List<ConsecutiveActivityBatchGroup> ConsecutiveActivityBatchGroups { get; set; } = new List<ConsecutiveActivityBatchGroup>();
    }
}
