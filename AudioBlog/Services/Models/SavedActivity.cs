﻿using AudlyApi.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Models
{
    public class SavedActivity
    {
        public ActivityQuerying.Models.Activity Activity { get; set; }

        public IEnumerable<SavedActivityDetails> SavedActivityDetails { get; set; } = new List<SavedActivityDetails>();
    }
}
