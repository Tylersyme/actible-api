﻿using AudlyApi.AppSettings;
using AudlyApi.Requests;
using AudlyApi.Services.Clients;
using AutoMapper;
using GoogleApi.Entities.Places.AutoComplete.Request;
using GoogleApi.Entities.Places.Common;
using GoogleApi.Entities.Places.Details.Request;
using GoogleApi.Entities.Places.Details.Response;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Controller
{
    public class PlaceControllerService : IPlaceControllerService
    {
        private readonly GoogleSettings _googleSettings;

        private readonly IMapper _mapper;

        private readonly IGooglePlacesClient _googlePlacesClient;

        public PlaceControllerService(
            IOptions<GoogleSettings> options,
            IMapper mapper,
            IGooglePlacesClient googlePlacesClient)
        {
            _googleSettings = options.Value;
            _mapper = mapper;
            _googlePlacesClient = googlePlacesClient;
        }

        public async Task<IEnumerable<Prediction>> GetPlaceAutocompletePredictionsAsync(PlaceAutocompleteRequest request)
        {
            var googleAutocompleteRequest = _mapper.Map<PlacesAutoCompleteRequest>(request);
            googleAutocompleteRequest.Key = _googleSettings.ApiKey;

            var predictions = await _googlePlacesClient.GetAutocompletePredictionsAsync(googleAutocompleteRequest);

            return predictions;
        }

        public async Task<DetailsResult> GetPlaceAutocompleteDetailsAsync(PlaceAutocompleteDetailsRequest request)
        {
            var googlePlacesDetailsRequest = _mapper.Map<PlacesDetailsRequest>(request);
            googlePlacesDetailsRequest.Key = _googleSettings.ApiKey;

            var details = await _googlePlacesClient.GetDetailsAsync(googlePlacesDetailsRequest);

            return details;
        }
    }
}
