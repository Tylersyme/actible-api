﻿using AudlyApi.Requests;
using GoogleApi.Entities.Places.Common;
using GoogleApi.Entities.Places.Details.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Controller
{
    public interface IPlaceControllerService
    {
        Task<IEnumerable<Prediction>> GetPlaceAutocompletePredictionsAsync(PlaceAutocompleteRequest request);

        Task<DetailsResult> GetPlaceAutocompleteDetailsAsync(PlaceAutocompleteDetailsRequest request);
    }
}
