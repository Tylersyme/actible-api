﻿using ActivityQuerying.Models;
using ActivityQuerying.Models.Components;
using AudlyApi.Requests;
using AudlyApi.Responses;
using AudlyApi.Services.Models;
using Entities.Activity;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Controller
{
    public interface IActivityControllerService
    {
        Task<IEnumerable<ActivityQuerying.Models.Activity>> GetNextActivitiesAsync(ActivityRequest activityRequest, Guid userId);

        Task<IEnumerable<Component>> GetActivityDetailComponentsAsync(ActivityDetailsRequest request);

        Task<IEnumerable<SavedActivity>> GetSavedActivitiesAsync(Guid userId, SavedActivitiesRequest request);

        Task<UserSavedActivitySourceMap> SaveActivityAsync(Guid userId, string activityIdentifier);

        Task<SavedActivityDetails> ScheduleActivityAsync(Guid userId, ScheduleActivityRequest request);

        Task UpdateScheduledActivityAsync(Guid userId, UpdateScheduledActivityRequest request);

        Task DeleteSavedActivityAsync(Guid userId, string activityIdentifier);
    }
}
