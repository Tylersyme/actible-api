﻿using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Models.Components;
using ActivityQuerying.Services;
using ActivityQuerying.Services.ActivityResults;
using AudlyApi.Extensions;
using AudlyApi.Requests;
using AudlyApi.Responses;
using AudlyApi.Services.Activity;
using AudlyApi.Services.Cache;
using AudlyApi.Services.Models;
using AutoMapper;
using Entities.Activity;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using Org.BouncyCastle.Ocsp;
using Persistence;
using Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace AudlyApi.Services.Controller
{
    public class ActivityControllerService : IActivityControllerService
    {
        private readonly IMapper _mapper;

        private readonly IActivityOrchestrator _activityOrchestrator;

        private readonly IActivityDetailsService _activityDetailsService;

        private readonly IActivitySourceRepository _activitySourceRepository;

        private readonly IUserViewedActivitySourceMapRepository _userViewedActivitySourceMapRepository;

        private readonly IUserSavedActivitySourceMapRepository _userSavedActivitySourceMapRepository;

        private readonly IActivityCacheManager _activityCacheManager;

        private readonly IActivityHistoryService _activityHistoryService;

        private readonly IActivityLookupService _yelpActivityLookupService;

        private readonly AudlyContext _audlyContext;

        public ActivityControllerService(
            IMapper mapper,
            IActivityOrchestrator activityOrchestrator,
            IActivityDetailsService activityDetailsService,
            IActivitySourceRepository activitySourceRepository,
            IUserViewedActivitySourceMapRepository userViewedActivitySourceMapRepository,
            IUserSavedActivitySourceMapRepository userSavedActivitySourceMapRepository,
            IActivityCacheManager activityCacheManager,
            IActivityHistoryService activityHistoryService,
            IActivityLookupService yelpActivityLookupService,
            AudlyContext audlyContext)
        {
            _mapper = mapper;
            _activityOrchestrator = activityOrchestrator;
            _activityDetailsService = activityDetailsService;
            _activitySourceRepository = activitySourceRepository;
            _userViewedActivitySourceMapRepository = userViewedActivitySourceMapRepository;
            _userSavedActivitySourceMapRepository = userSavedActivitySourceMapRepository;
            _activityCacheManager = activityCacheManager;
            _activityHistoryService = activityHistoryService;
            _yelpActivityLookupService = yelpActivityLookupService;
            _audlyContext = audlyContext;
        }

        public async Task<IEnumerable<ActivityQuerying.Models.Activity>> GetNextActivitiesAsync(ActivityRequest request, Guid userId)
        {
            var activityFilter = _mapper.Map<ActivityFilter>(request);

            var userActivityHistory = await _activityHistoryService.GetUserRecentActivityHistoryAsync(userId);

            var activityBatch = await _activityOrchestrator.GetNextActivityBatchAsync(userId, activityFilter, userActivityHistory);

            await this.AddUserViewedActivitySourcesAsync(userId, activityBatch.Activities);

            _activityCacheManager.PushActivityBatchDetails(userId, new ActivityBatchDetails
            {
                ActivityFilter = activityFilter,
                ActivityResultSummaries = activityBatch.ActivityResultSummary,
            });

            return activityBatch.Activities;
        }

        public async Task<IEnumerable<Component>> GetActivityDetailComponentsAsync(ActivityDetailsRequest request)
        {
            var filter = _mapper.Map<ActivityDetailsFilter>(request);

            var detailComponents = await _activityDetailsService.GetDetailComponentsAsync(filter);

            return detailComponents;
        }

        public async Task<IEnumerable<SavedActivity>> GetSavedActivitiesAsync(Guid userId, SavedActivitiesRequest request)
        {
            var userSavedActivitiesToLookup = (await _audlyContext
                .Set<UserSavedActivitySourceMap>()
                .Include(usm => usm.ActivitySource)
                .Include(usm => usm.SavedActivityDetails)
                .Where(usm => usm.UserId == userId)
                .ToListAsync())
                .Where(a => !request.ExcludedActivitySourceIdentifiers.Contains(a.ActivitySource.ActivityIdentifier));

            var savedActivities = new List<SavedActivity>();

            foreach (var userSavedActivity in userSavedActivitiesToLookup)
                savedActivities.Add(new SavedActivity
                {
                    Activity = await _yelpActivityLookupService.LookupActivityByIdentifierAsync(userSavedActivity.ActivitySource.ActivityIdentifier),
                    SavedActivityDetails = _mapper.Map<IEnumerable<RequestModels.SavedActivityDetails>>(userSavedActivity.SavedActivityDetails),
                });

            return savedActivities;
        }

        public async Task<UserSavedActivitySourceMap> SaveActivityAsync(Guid userId, string activityIdentifier)
        {
            var activitySource = await _userViewedActivitySourceMapRepository.GetActivitySourceByUserAndIdentifierAsync(userId, activityIdentifier);

            var userSavedActivitySourceMap = new UserSavedActivitySourceMap
            {
                UserId = userId,
                ActivitySourceId = activitySource.Id,
            };

            _audlyContext
                .Set<UserSavedActivitySourceMap>()
                .Add(userSavedActivitySourceMap);

            await _audlyContext.SaveChangesAsync();

            return userSavedActivitySourceMap;
        }

        public async Task<SavedActivityDetails> ScheduleActivityAsync(Guid userId, ScheduleActivityRequest request)
        {
            var userSavedActivity = await _userSavedActivitySourceMapRepository.GetSavedActivitySourceMapAsync(userId, request.ActivityIdentifier)
                ?? await this.SaveActivityAsync(userId, request.ActivityIdentifier);

            var savedActivityDetails = _mapper.Map<SavedActivityDetails>(request.SavedActivityDetails);
            savedActivityDetails.UserId = userId;
            savedActivityDetails.ActivitySourceId = userSavedActivity.ActivitySourceId;

            _audlyContext
                .Set<SavedActivityDetails>()
                .Add(savedActivityDetails);

            await _audlyContext.SaveChangesAsync();

            return savedActivityDetails;
        }

        public async Task DeleteSavedActivityAsync(Guid userId, string activityIdentifier) =>
            await _userSavedActivitySourceMapRepository.DeleteSavedActivitySourceMapAsync(userId, activityIdentifier);

        public async Task UpdateScheduledActivityAsync(Guid userId, UpdateScheduledActivityRequest request)
        {
            var existingSavedActivityDetails = await _audlyContext
                .Set<SavedActivityDetails>()
                .FirstAsync(sad => sad.Id == request.SavedActivityDetails.Id);

            // The user must be verified before allowing update
            if (existingSavedActivityDetails.UserId != userId)
                throw new UnauthorizedAccessException();

            _mapper.Map(request.SavedActivityDetails, existingSavedActivityDetails);

            _audlyContext.Update(existingSavedActivityDetails);

            await _audlyContext.SaveChangesAsync();
        }

        private async Task AddUserViewedActivitySourcesAsync(Guid userId, IEnumerable<ActivityQuerying.Models.Activity> activities)
        {
            var activitySources = activities.Select(a => new Entities.Activity.ActivitySource
            {
                ActivitySourceType = a.Source.ActivitySourceType,
                ActivityIdentifier = a.Source.Identifier,
            });

            await _activitySourceRepository.AddUniqueAsync(activitySources);

            await _userViewedActivitySourceMapRepository.AddOrUpdateFromActivityIdentifiersAsync(userId, activitySources.Select(a => a.ActivityIdentifier));
        }
    }
}
