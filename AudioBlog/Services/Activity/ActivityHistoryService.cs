﻿using ActivityQuerying.Filtering;
using AudlyApi.Services.Cache;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Activity
{
    public class ActivityHistoryService : IActivityHistoryService
    {
        private readonly IActivityCacheManager _activityCacheManager;

        public ActivityHistoryService(IActivityCacheManager activityCacheManager)
        {
            _activityCacheManager = activityCacheManager;
        }

        public Task<ActivityHistory> GetUserRecentActivityHistoryAsync(Guid userId)
        {
            var isCached = _activityCacheManager.TryGetActivityCacheData(userId, out var activityCacheData);

            // TODO: Fallback to database
            if (!isCached)
                return Task.FromResult(new ActivityHistory());

            return Task.FromResult(new ActivityHistory
            {
                RecentActivityBatchDetails = activityCacheData.RecentActivityBatchDetails,
                ConsecutiveActivityBatchGroups = activityCacheData.ConsecutiveActivityBatchGroups,
            });
        }
    }
}
