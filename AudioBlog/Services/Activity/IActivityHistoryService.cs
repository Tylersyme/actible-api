﻿using ActivityQuerying.Filtering;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Services.Activity
{
    public interface IActivityHistoryService
    {
        Task<ActivityHistory> GetUserRecentActivityHistoryAsync(Guid userId);
    }
}
