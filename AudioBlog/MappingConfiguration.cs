﻿using ActivityQuerying.Services;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi
{
    public static class MappingConfiguration
    {
        public static IMapper Mapper;

        public static void ConfigureMappings()
        {
            var assembly = typeof(MappingConfiguration).Assembly;
            var activityQueryingAssembly = typeof(ActivityOrchestrator).Assembly;

            Mapper = new MapperConfiguration(cfg =>
            {
                cfg.AddMaps(assembly);
                cfg.AddMaps(activityQueryingAssembly);
            })
            .CreateMapper();
        }
    }
}
