﻿using AudlyApi.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Requests
{
    public class UpdateScheduledActivityRequest
    {
        public SavedActivityDetails SavedActivityDetails { get; set; }
    }
}
