﻿using AudlyApi.RequestModels;
using AudlyApi.Responses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Requests
{
    public class ActivityRequest
    {
        public List<string> Tags { get; set; } = new List<string>();

        public GeolocationCoordinates GeolocationCoordinates { get; set; }
    }
}
