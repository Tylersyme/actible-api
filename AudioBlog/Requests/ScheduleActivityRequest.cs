﻿using AudlyApi.RequestModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Requests
{
    public class ScheduleActivityRequest
    {
        public string ActivityIdentifier { get; set; }

        public SavedActivityDetails SavedActivityDetails { get; set; }
    }
}
