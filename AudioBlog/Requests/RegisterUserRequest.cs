﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.RequestObjects
{
    public class RegisterUserRequest
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string PlainTextPassword { get; set; }
    }
}
