﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.RequestModels
{
    public class SavedActivityDetails
    {
        public Guid Id { get; set; }

        public DateTime StartTime { get; set; }

        public string Instructions { get; set; }
    }
}
