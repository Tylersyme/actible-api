using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using ActivityQuerying;
using AudlyApi;
using AudlyApi.AppSettings;
using AudlyApi.Middleware;
using AudlyApi.Services.Activity;
using AudlyApi.Services.Cache;
using AudlyApi.Services.Clients;
using AudlyApi.Services.Controller;
using Autofac;
using Autofac.Core;
using Autofac.Extensions.DependencyInjection;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.AzureADB2C.UI;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Identity.Web;
using Microsoft.IdentityModel.Tokens;
using Persistence;
using PlacesQuerying;

namespace AudioBlog
{
    public class Startup
    {
        private readonly string _corsDevelopmentName = "TemporaryDevelopmentPolicy";

        public Startup(IConfiguration configuration)
        {
            this.Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        public ILifetimeScope AutofacContainer { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            // TODO: Configure for production
            services.AddCors(opts => opts.AddPolicy(_corsDevelopmentName, builder =>
            {
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            }));

            MappingConfiguration.ConfigureMappings();

            var connectionString = this.Configuration
                .GetSection("ConnectionStrings")
                .Get<ConnectionStringSettingsModel>()
                .AudlyConnectionString;

            services.AddDbContext<AudlyContext>(opts => opts.UseSqlServer(connectionString));

            var activityCacheSettingsSection = this.Configuration.GetSection(ActivityCacheSettings.SECTION_NAME);
            services.Configure<ActivityCacheSettings>(activityCacheSettingsSection);

            var googleSettingsSection = this.Configuration.GetSection(GoogleSettings.SECTION_NAME);
            services.Configure<GoogleSettings>(googleSettingsSection);

            services
                //.AddAuthentication(AzureADB2CDefaults.BearerAuthenticationScheme)
                //.AddAzureADB2CBearer(opts => this.Configuration.Bind("AzureAdB2C", opts));
                .AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddMicrosoftIdentityWebApi(opts =>
                {
                    this.Configuration.Bind("AzureAdB2C", opts);

                    //opts.TokenValidationParameters.NameClaimType = "name";
                },
                opts => { this.Configuration.Bind("AzureAdB2C", opts); });

            //services
            //    .AddAuthentication(opts =>
            //    {
            //        opts.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //        opts.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //    })
            //    .AddJwtBearer(config =>
            //    {
            //        config.RequireHttpsMetadata = false;
            //        config.SaveToken = true;
            //        config.TokenValidationParameters = new TokenValidationParameters
            //        {
            //            ValidateIssuer = true,
            //            ValidateAudience = true,
            //            ValidateLifetime = true,
            //            ValidateIssuerSigningKey = true,
            //            IssuerSigningKey = key,
            //            ValidIssuer = AuthConstants.Issuer,
            //            ValidAudience = AuthConstants.Audience,
            //        };
            //    });

            services.AddAuthorization();

            services.AddDistributedMemoryCache();

            services.AddSession();

            services.AddControllers();

            services.AddMemoryCache();
        }

        public void ConfigureContainer(ContainerBuilder builder)
        {
            builder
                .RegisterType<ActivityControllerService>()
                .As<IActivityControllerService>();

            builder
                .RegisterType<ActivityCacheManager>()
                .As<IActivityCacheManager>();

            builder
                .Register(_ => MappingConfiguration.Mapper)
                .AsSelf();

            builder
                .RegisterType<ActivityHistoryService>()
                .As<IActivityHistoryService>();

            builder
                .Register(_ => this.Configuration
                    .GetSection("ConnectionStrings")
                    .Get<ConnectionStringSettingsModel>())
                .AsSelf();

            builder
                .Register(ctx =>
                {
                    var connectionString = ctx.Resolve<ConnectionStringSettingsModel>().AudlyConnectionString;

                    //var dbContextOptionsBuilder = new DbContextOptionsBuilder<AudlyContext>()
                    //    .UseSqlServer(connectionString);

                    var dbContextOptionsBuilder = new DbContextOptionsBuilder<AudlyContext>()
                        .UseMySql(connectionString);

                    return new AudlyContext(dbContextOptionsBuilder.Options);
                })
                .AsSelf()
                .InstancePerLifetimeScope();

            builder
                .RegisterType<PlaceControllerService>()
                .As<IPlaceControllerService>();

            builder
                .RegisterType<GooglePlacesClient>()
                .As<IGooglePlacesClient>();

            builder
                .RegisterModule(new RepositoryModule())
                .RegisterModule(new ActivityQueryingModule())
                .RegisterModule(new PlacesQueryingModule());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            this.AutofacContainer = app.ApplicationServices.GetAutofacRoot();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseExceptionHandler(error =>
            {
                error.Run(async context =>
                {
                    var contextFeature = context.Features.Get<IExceptionHandlerFeature>();
                });
            });

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseCors(_corsDevelopmentName);

            app.UseAuthentication();

            app.UseAuthorization();

            app.UseSession();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
