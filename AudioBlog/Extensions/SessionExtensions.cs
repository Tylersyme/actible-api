﻿using ActivityQuerying.Models;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace AudlyApi.Extensions
{
    public static class SessionExtensions
    {
        public static void Set<TValue>(this ISession session, string key, TValue value) =>
            session.SetString(key, JsonSerializer.Serialize(value));

        public static TValue Get<TValue>(this ISession session, string key) =>
            JsonSerializer.Deserialize<TValue>(session.GetString(key));
    }
}
