﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudlyApi.Extensions
{
    public static class ControllerBaseExtensions
    {
        public static StatusCodeResult InternalServerError(this ControllerBase controllerBase) =>
            controllerBase.StatusCode(500);
    }
}
