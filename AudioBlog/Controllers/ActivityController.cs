﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using ActivityQuerying.Enums;
using ActivityQuerying.Exceptions;
using ActivityQuerying.Filtering;
using ActivityQuerying.Models;
using ActivityQuerying.Services;
using AudlyApi.Extensions;
using AudlyApi.Requests;
using AudlyApi.Responses;
using AudlyApi.Services.Controller;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AudlyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ActivityController : ControllerBase
    {
        private readonly IActivityControllerService _activityControllerService;

        public ActivityController(IActivityControllerService activityControllerService)
        {
            _activityControllerService = activityControllerService;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetNextActivities(string requestJson)
        {
            try
            {
                var activityRequest = JsonSerializer.Deserialize<ActivityRequest>(requestJson);

                var activities = await _activityControllerService.GetNextActivitiesAsync(activityRequest, base.User.GetId());

                var responses = activities
                    .Select(s => new ActivityResponse
                    {
                        Activity = s,
                    });
                
                return base.Ok(responses);
            }
            catch (Exception ex)
            {
                return this.InternalServerError();
            }
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetActivityDetails(string requestJson)
        {
            try
            {
                var activityDetailsRequest = JsonSerializer.Deserialize<ActivityDetailsRequest>(requestJson);

                var detailComponents = await _activityControllerService.GetActivityDetailComponentsAsync(activityDetailsRequest);

                var response = new ActivityDetailsResponse
                {
                    DetailComponents = detailComponents,
                };

                return base.Ok(response);
            }
            catch (DetailsSearchResultsNotFoundException)
            {
                // TODO: Return base.NotFound
                return base.Ok(new ActivityDetailsResponse());
            }
            catch (Exception ex)
            {
                return this.InternalServerError();
            }
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetSavedActivities(string requestJson)
        {
            try
            {
                var savedActivitiesRequest = JsonSerializer.Deserialize<SavedActivitiesRequest>(requestJson);

                var savedActivities = await _activityControllerService.GetSavedActivitiesAsync(base.User.GetId(), savedActivitiesRequest);

                var response = new SavedActivitiesResponse
                {
                    SavedActivities = savedActivities,
                };

                return base.Ok(response);
            }
            catch (Exception ex)
            {
                return this.InternalServerError();
            }
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> SaveActivity(string activityIdentifier)
        {
            try
            {
                await _activityControllerService.SaveActivityAsync(base.User.GetId(), activityIdentifier);

                return base.Ok();
            }
            catch (Exception ex)
            {
                return this.InternalServerError();
            }
        }

        [Route("[action]")]
        [HttpPost]
        public async Task<IActionResult> ScheduleActivity([FromBody] ScheduleActivityRequest request)
        {
            try
            {
                var savedActivityDetails = await _activityControllerService.ScheduleActivityAsync(base.User.GetId(), request);

                var response = new ScheduleActivityResponse
                {
                    SavedActivityDetailsId = savedActivityDetails.Id,
                };

                return base.Ok(response);
            }
            catch (Exception ex)
            {
                return this.InternalServerError();
            }
        }

        [Route("[action]")]
        [HttpPut]
        public async Task<IActionResult> UpdateScheduledActivity([FromBody] UpdateScheduledActivityRequest request)
        {
            try
            {
                await _activityControllerService.UpdateScheduledActivityAsync(base.User.GetId(), request);

                return base.Ok();
            }
            catch (UnauthorizedAccessException)
            {
                return base.Unauthorized();
            }
            catch (Exception ex)
            {
                return this.InternalServerError();
            }
        }

        [Route("[action]")]
        [HttpDelete]
        public async Task<IActionResult> DeleteSavedActivity(string activityIdentifier)
        {
            try
            {
                await _activityControllerService.DeleteSavedActivityAsync(base.User.GetId(), activityIdentifier);

                return base.Ok();
            }
            catch (Exception ex)
            {
                return this.InternalServerError();
            }
        }
    }
}
