﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;
using AudlyApi.Extensions;
using AudlyApi.Requests;
using AudlyApi.Responses;
using AudlyApi.Services.Controller;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AudlyApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PlaceController : ControllerBase
    {
        private readonly IPlaceControllerService _placeControllerService;

        private readonly IMapper _mapper;

        public PlaceController(
            IPlaceControllerService placeControllerService,
            IMapper mapper)
        {
            _placeControllerService = placeControllerService;
            _mapper = mapper;
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetAutocompletePredictions(string requestJson)
        {
            try
            {
                var request = JsonSerializer.Deserialize<PlaceAutocompleteRequest>(requestJson);

                var predictions = await _placeControllerService.GetPlaceAutocompletePredictionsAsync(request);

                var predictionResponses = _mapper.Map<IEnumerable<PlaceAutocompleteResponse>>(predictions);

                return base.Ok(predictionResponses);
            }
            catch (Exception)
            {
                return this.InternalServerError();
            }
        }

        [Route("[action]")]
        [HttpGet]
        public async Task<IActionResult> GetAutocompleteDetails(string requestJson)
        {
            try
            {
                var request = JsonSerializer.Deserialize<PlaceAutocompleteDetailsRequest>(requestJson);

                var detailsResult = await _placeControllerService.GetPlaceAutocompleteDetailsAsync(request);

                var response = _mapper.Map<PlaceAutocompleteDetailsResponse>(detailsResult);

                return base.Ok(response);
            }
            catch (Exception)
            {
                return this.InternalServerError();
            }
        }
    }
}
