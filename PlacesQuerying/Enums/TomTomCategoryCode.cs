﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlacesQuerying.Enums
{
    /// <summary>
    /// The tom tom category codes found at: https://developer.tomtom.com/search-api/search-api-documentation/supported-category-codes
    /// </summary>
    public class TomTomCategoryCode
    {
        public static readonly List<TomTomCategoryCode> ALL = new List<TomTomCategoryCode>();

        public static readonly TomTomCategoryCode Unsupported = new TomTomCategoryCode("unsupported", -1);

        public static readonly TomTomCategoryCode AdventureSportsVenue = new TomTomCategoryCode("adventure_sports_venue", 7305);
        public static readonly TomTomCategoryCode AmusementPark = new TomTomCategoryCode("amusement_park", 9902);
        public static readonly TomTomCategoryCode Beach = new TomTomCategoryCode("beach", 9357);
        public static readonly TomTomCategoryCode CafePub = new TomTomCategoryCode("cafe_pub", 9376);
        public static readonly TomTomCategoryCode CampingGround = new TomTomCategoryCode("camping_ground", 7360);
        //public static readonly TomTomCategoryCode CarWash = new TomTomCategoryCode("car_wash", 9155);
        public static readonly TomTomCategoryCode Casino = new TomTomCategoryCode("casino", 7341);
        public static readonly TomTomCategoryCode Cinema = new TomTomCategoryCode("cinema", 7342);
        public static readonly TomTomCategoryCode ClubAssociation = new TomTomCategoryCode("club_association", 9937);
        public static readonly TomTomCategoryCode CommunityCenter = new TomTomCategoryCode("community_center", 7363);
        public static readonly TomTomCategoryCode CulturalCenter = new TomTomCategoryCode("cultural_center", 7319);
        public static readonly TomTomCategoryCode DepartmentStore = new TomTomCategoryCode("department_store", 7327);
        public static readonly TomTomCategoryCode Exchange = new TomTomCategoryCode("exchange", 9160);
        public static readonly TomTomCategoryCode ExhibitionConventionCenter = new TomTomCategoryCode("exhibition_convention_center", 9377);
        public static readonly TomTomCategoryCode GolfCourse = new TomTomCategoryCode("golf_course", 9911);
        //public static readonly TomTomCategoryCode HolidayRental = new TomTomCategoryCode("holiday_rental", 7304);
        public static readonly TomTomCategoryCode IceSkatingRink = new TomTomCategoryCode("ice_skating_rink", 9360);
        public static readonly TomTomCategoryCode ImportantTouristAttraction = new TomTomCategoryCode("important_tourist_attraction", 7376);
        // TODO: Excellent most difficult case scenario for verbs
        public static readonly TomTomCategoryCode LeisureCenter = new TomTomCategoryCode("leisure_center", 9378);
        public static readonly TomTomCategoryCode Library = new TomTomCategoryCode("library", 9913);
        public static readonly TomTomCategoryCode Market = new TomTomCategoryCode("market", 7332);
        public static readonly TomTomCategoryCode Museum = new TomTomCategoryCode("museum", 7317);
        public static readonly TomTomCategoryCode Nightlife = new TomTomCategoryCode("nightlife", 9379);
        public static readonly TomTomCategoryCode ParkRecreationArea = new TomTomCategoryCode("park_recreation_area", 9362);
        public static readonly TomTomCategoryCode PlaceOfWorship = new TomTomCategoryCode("place_of_worship", 7339);
        //public static readonly TomTomCategoryCode ResidentialAccommodation = new TomTomCategoryCode("residential_accommodation", 7303);
        public static readonly TomTomCategoryCode Restaurant = new TomTomCategoryCode("restaurant", 7315);
        public static readonly TomTomCategoryCode ScenicParnoramicView = new TomTomCategoryCode("scenic_panoramic_view", 7337);
        public static readonly TomTomCategoryCode Shop = new TomTomCategoryCode("shop", 9361);
        public static readonly TomTomCategoryCode ShoppingCenter = new TomTomCategoryCode("shopping_center", 7373);
        public static readonly TomTomCategoryCode SportsCenter = new TomTomCategoryCode("sports_center", 7320);
        public static readonly TomTomCategoryCode Stadium = new TomTomCategoryCode("stadium", 7374);
        public static readonly TomTomCategoryCode SwimmingPool = new TomTomCategoryCode("swimming_pool", 7338);
        public static readonly TomTomCategoryCode TennisCourt = new TomTomCategoryCode("tennis_court", 9369);
        public static readonly TomTomCategoryCode Theater = new TomTomCategoryCode("theater", 7318);
        public static readonly TomTomCategoryCode TrailSystem = new TomTomCategoryCode("trail_system", 7302);
        //public static readonly TomTomCategoryCode WaterSport = new TomTomCategoryCode("water_sport", 9371);
        public static readonly TomTomCategoryCode Winery = new TomTomCategoryCode("winery", 7349);
        public static readonly TomTomCategoryCode ZoosArboretaBotanicalGarden = new TomTomCategoryCode("zoos_arboreta_botanical_garden", 9927);

        // TODO: Is this redundant with restaurant?
        //public static readonly TomTomCategoryCode RestaurantArea = new TomTomCategoryCode("restaurant_area");

        // TODO: Does fishing apply?
        //public static readonly TomTomCategoryCode Marina = new TomTomCategoryCode("marina");

        // TODO: How much, if any, of this should be used?
        //public static readonly TomTomCategoryCode GeographicFeature = new TomTomCategoryCode("geographic_feature");

        // TODO: This is vague, is this desireable?
        //public static readonly TomTomCategoryCode Entertainment = new TomTomCategoryCode("entertainment");

        public string Code { get; set; }

        public int Id { get; set; }

        private TomTomCategoryCode(string code, int id)
        {
            this.Code = code;

            this.Id = id;

            ALL.Add(this);
        }

        public static TomTomCategoryCode GetByStringOrUnsupported(string code) =>
            ALL.FirstOrDefault(c => c.Code.ToUpper() == code.ToUpper()) ?? Unsupported;
    }
}
