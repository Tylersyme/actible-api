﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Enums
{
    public class TomTomResponseFormat
    {
        public static readonly TomTomResponseFormat Json = new TomTomResponseFormat("json");

        /// <summary>
        /// Gets the name of the tom tom response format to be used in the request url.
        /// </summary>
        public string Name { get; private set; }

        private TomTomResponseFormat(string name)
        {
            this.Name = name;
        }
    }
}
