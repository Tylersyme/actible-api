﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Enums
{
    public class TomTomSearchMethod
    {
        public static readonly TomTomSearchMethod NearbySearch = new TomTomSearchMethod("nearbySearch");

        /// <summary>
        /// Gets or the url segment which should be used in a tom tom api search url.
        /// </summary>
        public string UrlSegment { get; private set; }

        private TomTomSearchMethod(string urlSegment)
        {
            this.UrlSegment = urlSegment;
        }
    }
}
