﻿using PlacesQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Requests.TomTom.Search
{
    public abstract class TomTomSearchRequest
    {
        /// <summary>
        /// Gets or sets the tom tom api key for authentication.
        /// </summary>
        public string ApiKey { get; set; }

        /// <summary>
        /// Gets or sets the version of the tom tom api being used.
        /// </summary>
        public int VersionNumber { get; set; }

        /// <summary>
        /// Gets or sets the search method which the tom tom api will use to search for results.
        /// </summary>
        public abstract TomTomSearchMethod SearchMethod { get; }

        /// <summary>
        /// Gets or sets the response format which the tom tom api will respond with.
        /// </summary>
        public TomTomResponseFormat ResponseFormat { get; set; }

        /// <summary>
        /// Gets or sets the latitude where results should be biased.
        /// </summary>
        public float Latitude { get; set; }

        /// <summary>
        /// Gets or sets the longitude where results should be biased.
        /// </summary>
        public float Longitude { get; set; }

        /// <summary>
        /// Gets or sets a comma separated list of category ids used to restrict/filter the results.
        /// </summary>
        public string CategorySet { get; set; }
    }
}
