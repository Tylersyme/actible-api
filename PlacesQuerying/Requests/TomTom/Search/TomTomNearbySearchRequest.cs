﻿using PlacesQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Requests.TomTom.Search
{
    public class TomTomNearbySearchRequest : TomTomSearchRequest
    {
        public override TomTomSearchMethod SearchMethod { get => TomTomSearchMethod.NearbySearch; }

        /// <summary>
        /// Gets or sets the search radius in meters.
        /// </summary>
        public int? Radius { get; set; }

        /// <summary>
        /// The maximum number of responses that will be returned.
        /// Default value: 10
        /// Maximum value: 100
        /// </summary>
        public int? ResultLimit { get; set; }

        /// <summary>
        /// Starting offset of the returned results within the full result set.
        /// Default value: 0
        /// Maximum value: 1900
        /// </summary>
        public int? ResultOffset { get; set; }
    }
}
