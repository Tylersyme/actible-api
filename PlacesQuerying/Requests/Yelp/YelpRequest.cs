﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Requests.Yelp
{
    public class YelpRequest
    {
        public string Version { get; set; }

        public string ApiKey { get; set; }
    }
}
