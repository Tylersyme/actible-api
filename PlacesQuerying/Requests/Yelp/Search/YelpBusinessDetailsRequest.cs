﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Requests.Yelp.Search
{
    public class YelpBusinessDetailsRequest : YelpRequest
    {
        /// <summary>
        /// The unique id used to lookup the business.
        /// </summary>
        public string BusinessId { get; set; }

        /// <summary>
        /// Optional. Specify the locale into which to localize the business information. Defaults to en_US.
        /// </summary>
        public string Locale { get; set; }
    }
}
