﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Requests.Yelp.Search
{
    public class YelpBusinessSearchRequest : YelpRequest
    {
        /// <summary>
        /// Optional. Search term, for example "food" or "restaurants". 
        /// The term may also be business names, such as "Starbucks". 
        /// If term is not included the endpoint will default to searching across businesses from a small number of popular categories.
        /// </summary>
        public string Term { get; set; }

        /// <summary>
        ///	Required if either latitude or longitude is not provided. 
        ///	This string indicates the geographic area to be used when searching for businesses. 
        ///	Examples: "New York City", "NYC", "350 5th Ave, New York, NY 10118". 
        ///	Businesses returned in the response may not be strictly within the specified location. 
        /// </summary>
        public string Location { get; set; }

        /// <summary>
        /// Optional. Categories to filter the search results with.
        /// The category filter can be a list of comma delimited categories. For example, "bars,french" will filter by Bars OR French. 
        /// The category identifier should be used (for example "discgolf", not "Disc Golf").
        /// </summary>
        public IEnumerable<string> Categories { get; set; } = new List<string>();

        /// <summary>
        /// Optional. Offset the list of returned business results by this amount.
        /// </summary>
        public int? Offset { get; set; }

        /// <summary>
        /// Required if location is not provided. 
        /// Latitude of the location you want to search nearby. 
        /// </summary>
        public float? Latitude { get; set; }

        /// <summary>
        /// Required if location is not provided. Latitude of the location you want to search nearby.
        /// </summary>
        public float? Longitude { get; set; }

        /// <summary>
        /// Optional. A suggested search radius in meters. 
        /// This field is used as a suggestion to the search. 
        /// The actual search radius may be lower than the suggested radius in dense urban areas, and higher in regions of less business density. 
        /// If the specified value is too large, a AREA_TOO_LARGE error may be returned. 
        /// The max value is 40000 meters (about 25 miles).
        /// </summary>
        public int? Radius { get; set; }

        /// <summary>
        /// Optional. Number of business results to return. By default, it will return 20. 
        /// Maximum is 50.
        /// </summary>
        public int? Limit { get; set; }
    }
}
