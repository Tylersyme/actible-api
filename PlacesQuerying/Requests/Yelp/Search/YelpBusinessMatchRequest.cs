﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Requests.Yelp.Search
{
    public class YelpBusinessMatchRequest : YelpRequest
    {
        /// <summary>
        /// Required. The name of the business. Maximum length is 64; only digits, letters, spaces, and !#$%&+,­./:?@'are allowed.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Required. The first line of the business’s address. 
        /// Maximum length is 64; only digits, letters, spaces, and ­’/#&,.: are allowed. 
        /// The empty string "" is allowed; this will specifically match certain service businesses that have no street address.
        /// </summary>
        public string Address1 { get; set; }

        /// <summary>
        /// Required. The city of the business. Maximum length is 64; only digits, letters, spaces, and ­’.() are allowed.
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Required. The ISO 3166-2 (with a few exceptions) state code of this business. Maximum length is 3.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Required. The ISO 3166-1 alpha-2 country code of this business. Maximum length is 2.
        /// </summary>
        public string Country { get; set; }
    }
}
