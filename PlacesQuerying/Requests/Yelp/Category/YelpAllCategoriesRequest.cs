﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Requests.Yelp.Category
{
    public class YelpAllCategoriesRequest : YelpRequest
    {
        /// <summary>
        /// Optional. Specify the locale to filter the categories returned to only those available in that locale, and to translate the names of the categories appropriately. 
        /// See the list of supported locales. If not included, all categories across all locales will be returned and the category names will be in English.
        /// </summary>
        public string Locale { get; set; }
    }
}
