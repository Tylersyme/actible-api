﻿using PlacesQuerying.Enums;
using PlacesQuerying.Services.Converters.Json;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PlacesQuerying.Responses.TomTom.Search
{
    public class TomTomSearchResponse
    {
        public SearchSummary Summary { get; set; }

        [JsonPropertyName("results")]
        public IList<TomTomSearchResult> SearchResults { get; set; } = new List<TomTomSearchResult>();
    }

    public class SearchSummary
    {
        /// <summary>
        /// Total number of results found.
        /// </summary>
        [JsonPropertyName("totalResults")]
        public int TotalPossibleResults { get; set; }
    }

    public class TomTomSearchResult
    {
        /// <summary>
        /// Type of the result (e.g. POI).
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// A stable unique id for the POI index, and a non-stable unique id for the other indexes. 
        /// Note: Stable id means that it doesn't change between data releases without changing the location, attribution or classification.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// The relevance of the result.
        /// A larger score means the Result has a higher probability of meeting the query criteria.
        /// </summary>
        [JsonPropertyName("score")]
        public double RelevanceScore { get; set; }

        /// <summary>
        /// This is the distance in meters if geobias was provided.
        /// </summary>
        [JsonPropertyName("dist")]
        public double Distance { get; set; }

        /// <summary>
        /// Optional section.
        /// Only present if type == Geography.
        /// </summary>
        public string EntityType { get; set; }

        /// <summary>
        /// Optional section. Only present if type == POI.
        /// </summary>
        [JsonPropertyName("poi")]
        public PointOfInterest PointOfInterest { get; set; }

        [JsonPropertyName("position")]
        public WorldPosition WorldPosition { get; set; }
    }

    public class WorldPosition
    {
        [JsonPropertyName("lat")]
        public float Latitude { get; set; }

        [JsonPropertyName("lon")]
        public float Longitude { get; set; }
    }

    public class PointOfInterest
    {
        /// <summary>
        /// Name of the point of interest.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The telephone number.
        /// </summary>
        [JsonPropertyName("phone")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// The url of an associated website.
        /// </summary>
        [JsonPropertyName("url")]
        public string WebsiteUrl { get; set; }

        /// <summary>
        /// The list of the most specific POI categories.
        /// </summary>
        [JsonPropertyName("categorySet")]
        public List<Category> Categories { get; set; } = new List<Category>();

        /// <summary>
        /// The classifications the point of interest belongs to.
        /// </summary>
        public List<Classification> Classifications { get; set; } = new List<Classification>();
    }

    public class Category
    {
        /// <summary>
        /// Category id.
        /// </summary>
        public int Id { get; set; }
    }

    public class Classification
    {
        /// <summary>
        /// Fixed top level category code.
        /// </summary>
        [JsonConverter(typeof(TomTomCategoryCodeConverter))]
        public TomTomCategoryCode Code { get; set; }

        /// <summary>
        /// The classification names which apply to the classification.
        /// </summary>
        [JsonPropertyName("names")]
        public List<Subcategory> Subcategories { get; set; } = new List<Subcategory>();
    }

    public class Subcategory
    {
        /// <summary>
        /// The locale of the name.
        /// </summary>
        public string NameLocale { get; set; }

        /// <summary>
        /// The classification name.
        /// </summary>
        public string Name { get; set; }
    }
}
