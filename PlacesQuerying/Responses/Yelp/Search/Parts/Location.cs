﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PlacesQuerying.Responses.Yelp.Search.Parts
{
    public class Location
    {
        public string City { get; set; }

        public string Country { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }

        public string Address3 { get; set; }

        public string State { get; set; }

        [JsonPropertyName("zip_code")]
        public string ZipCode { get; set; }

        [JsonPropertyName("display_address")]
        public IEnumerable<string> DisplayAddresses { get; set; } = new List<string>();
    }
}
