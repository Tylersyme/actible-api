﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Responses.Yelp.Search.Parts
{
    public class Category
    {
        /// <summary>
        /// Gets or sets the unique internal alias of the category.
        /// </summary>
        public string Alias { get; set; }

        /// <summary>
        /// Gets or sets the human readable title of the category.
        /// </summary>
        public string Title { get; set; }
    }
}
