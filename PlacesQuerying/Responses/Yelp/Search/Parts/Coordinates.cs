﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Responses.Yelp.Search.Parts
{
    public class Coordinates
    {
        public float Latitude { get; set; }

        public float Longitude { get; set; }
    }
}
