﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Responses.Yelp.Search
{
    public class YelpBusinessMatchResponse
    {
        /// <summary>
        /// Name of this business.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Unique Yelp ID of this business.
        /// </summary>
        public string Id { get; set; }
    }
}
