﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PlacesQuerying.Responses.Yelp.Search
{
    public class YelpBusinessSearchResponse
    {
        /// <summary>
        /// Total number of business Yelp finds based on the search criteria. 
        /// Sometimes, the value may exceed 1000. 
        /// In such case, you still can only get up to 1000 businesses using multiple queries and combinations of the "limit" and "offset" parameters.
        /// </summary>
        [JsonPropertyName("total")]
        public int TotalResults { get; set; }

        /// <summary>
        /// List of businesses Yelp finds based on the search criteria.
        /// </summary>
        [JsonPropertyName("businesses")]
        public List<YelpBusinessResult> BusinessResults { get; set; }
    }
}
