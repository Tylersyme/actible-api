﻿using PlacesQuerying.Responses.Yelp.Search.Parts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PlacesQuerying.Responses.Yelp.Search
{
    public class YelpBusinessResult
    {
        /// <summary>
        /// Unique Yelp ID of this business. Example: '4kMBvIEWPxWkWKFN__8SxQ'
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Rating for this business (value ranges from 1, 1.5, ... 4.5, 5).
        /// </summary>
        public float Rating { get; set; }

        /// <summary>
        /// Number of reviews for this business.
        /// </summary>
        [JsonPropertyName("review_count")]
        public int ReviewCount { get; set; }

        /// <summary>
        /// Price level of the business. Value is one of $, $$, $$$ and $$$$.
        /// </summary>
        public string Price { get; set; }

        /// <summary>
        /// URL of photo for this business.
        /// </summary>
        [JsonPropertyName("image_url")]
        public string ImageUrl { get; set; }

        /// <summary>
        /// Phone number of the business.
        /// </summary>
        [JsonPropertyName("phone")]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// NAme of this business.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// URL for business page on Yelp.
        /// </summary>
        [JsonPropertyName("url")]
        public string YelpUrl { get; set; }

        /// <summary>
        /// Whether business has been (permanently) closed.
        /// </summary>
        [JsonPropertyName("is_closed")]
        public bool IsPermanentlyClosed { get; set; }

        /// <summary>
        /// The distance in meters from the search location. This returns meters regardless of the locale.
        /// </summary>
        public decimal Distance { get; set; }

        /// <summary>
        /// The comma separated list of categories which the place belongs to.
        /// </summary>
        public IEnumerable<Parts.Category> Categories { get; set; }

        /// <summary>
        /// Coordinates of this business.
        /// </summary>
        public Coordinates Coordinates { get; set; }

        /// <summary>
        /// Location of this business, including address, city, state, zip code and country.
        /// </summary>
        public Location Location { get; set; }
    }
}
