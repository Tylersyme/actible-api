﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.Json.Serialization;

namespace PlacesQuerying.Responses.Yelp.Category
{
    public class YelpAllCategoriesResponse
    {
        public List<YelpCategory> Categories { get; set; } = new List<YelpCategory>();
    }

    public class YelpCategory
    {
        public string Alias { get; set; }

        public string Title { get; set; }

        [JsonPropertyName("parent_aliases")]
        public List<string> ParentAliases { get; set; }

        [JsonPropertyName("country_whitelist")]
        public List<string> CountryWhitelist { get; set; }

        [JsonPropertyName("country_blacklist")]
        public List<string> CountryBlacklist { get; set; }
    }
}
