﻿using PlacesQuerying.Responses.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlacesQuerying.Extensions
{
    public static class ClassificationExtensions
    {
        public static IEnumerable<string> GetSubcategoryNames(this Classification classification) =>
            classification.Subcategories.Select(s => s.Name);

        public static bool SubcategoryNameContains(this Classification classification, string value) =>
            classification
                .GetSubcategoryNames()
                .Any(n => n.Contains(value));

        public static bool SubcategoryNameContainsAny(this Classification classification, params string[] values) =>
            values.Any(v => classification.SubcategoryNameContains(v));
    }
}
