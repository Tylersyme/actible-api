﻿using Autofac;
using PlacesQuerying.Requests.TomTom.Search;
using PlacesQuerying.Services.Builders;
using PlacesQuerying.Services.Builders.TomTom;
using PlacesQuerying.Services.Builders.TomTom.NearbySearch;
using PlacesQuerying.Services.Clients;
using PlacesQuerying.Services.Converters;
using PlacesQuerying.Services.Converters.Urls;
using PlacesQuerying.Services.Converters.Urls.Factories;
using PlacesQuerying.Services.Converters.Urls.TomTom;
using PlacesQuerying.Services.Converters.Urls.Yelp;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying
{
    public class PlacesQueryingModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<TomTomNearbyRequestUrlConverter>()
                .As<IUrlConverterStrategyBase>();

            builder
                .RegisterType<YelpBusinessSearchRequestUrlConverter>()
                .As<IUrlConverterStrategyBase>();

            builder
                .RegisterType<YelpBusinessMatchRequestUrlConverter>()
                .As<IUrlConverterStrategyBase>();

            builder
                .RegisterType<YelpAllCategoriesRequestUrlConverter>()
                .As<IUrlConverterStrategyBase>();

            builder
                .RegisterType<YelpBusinessDetailsRequestUrlConverter>()
                .As<IUrlConverterStrategyBase>();

            builder
                .RegisterType<TomTomClient>()
                .As<ITomTomClient>();

            builder
                .RegisterType<YelpClient>()
                .As<IYelpClient>();

            builder
                .RegisterType<TomTomNearbySearchRequestBuilder>()
                .As<ITomTomNearbySearchRequestBuilder>();

            builder
                .RegisterType<UrlFactory>()
                .As<IUrlFactory>();
        }
    }
}
