﻿using Flurl;
using PlacesQuerying.Requests.Yelp.Category;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.Yelp
{
    public class YelpAllCategoriesRequestUrlConverter : IUrlConverterStrategy<YelpAllCategoriesRequest>
    {
        private static readonly string ALL_CATEGORIES_ENDPOINT = "categories";

        public Url ToUrl(Url baseEndpoint, YelpAllCategoriesRequest request)
        {
            var url = baseEndpoint
                .AppendPathSegment(request.Version)
                .AppendPathSegment(ALL_CATEGORIES_ENDPOINT)
                .SetQueryParams(new
                {
                    locale = request.Locale,
                });

            return url;
        }

        public Url ToUrl(Url baseEndpoint, object request) =>
            this.ToUrl(baseEndpoint, (YelpAllCategoriesRequest)request);
    }
}
