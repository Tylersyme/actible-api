﻿using Flurl;
using PlacesQuerying.Requests.Yelp.Search;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.Yelp
{
    public class YelpBusinessMatchRequestUrlConverter : IUrlConverterStrategy<YelpBusinessMatchRequest>
    {
        private static readonly string BUSINESS_MATCH_ENDPOINT = "businesses/matches";

        public Url ToUrl(Url baseEndpoint, YelpBusinessMatchRequest request)
        {
            var url = baseEndpoint
                .AppendPathSegment(request.Version)
                .AppendPathSegment(BUSINESS_MATCH_ENDPOINT)
                .SetQueryParams(new
                {
                    name = request.Name,
                    address1 = request.Address1,
                    city = request.City,
                    state = request.State,
                    country = request.Country,
                });

            return url;
        }

        public Url ToUrl(Url baseEndpoint, object request) =>
            this.ToUrl(baseEndpoint, (YelpBusinessMatchRequest)request);
    }
}
