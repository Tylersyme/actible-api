﻿using Flurl;
using PlacesQuerying.Requests.Yelp.Search;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.Yelp
{
    public class YelpBusinessSearchRequestUrlConverter : IUrlConverterStrategy<YelpBusinessSearchRequest>
    {
        private static readonly string BUSINESS_SEARCH_ENDPOINT = "businesses/search";

        public Url ToUrl(Url baseEndpoint, YelpBusinessSearchRequest request)
        {
            var url = baseEndpoint
                .AppendPathSegment(request.Version)
                .AppendPathSegment(BUSINESS_SEARCH_ENDPOINT)
                .SetQueryParams(new
                {
                    term = request.Term,
                    location = request.Location,
                    latitude = request.Latitude,
                    longitude = request.Longitude,
                    radius = request.Radius,
                    limit = request.Limit,
                    categories = string.Join(",", request.Categories),
                    offset = request.Offset,
                });

            return url;
        }

        public Url ToUrl(Url baseEndpoint, object request) =>
            this.ToUrl(baseEndpoint, (YelpBusinessSearchRequest)request);
    }
}
