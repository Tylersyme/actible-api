﻿using Flurl;
using PlacesQuerying.Requests.Yelp.Search;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.Yelp
{
    public class YelpBusinessDetailsRequestUrlConverter : IUrlConverterStrategy<YelpBusinessDetailsRequest>
    {
        private static readonly string BUSINESS_DETAILS_ENDPOINT = "businesses";

        public Url ToUrl(Url baseEndpoint, YelpBusinessDetailsRequest request)
        {
            var url = baseEndpoint
                .AppendPathSegment(request.Version)
                .AppendPathSegment(BUSINESS_DETAILS_ENDPOINT)
                .AppendPathSegment(request.BusinessId)
                .SetQueryParams(new
                {
                    locale = request.Locale,
                });

            return url;
        }

        public Url ToUrl(Url baseEndpoint, object request) =>
            this.ToUrl(baseEndpoint, (YelpBusinessDetailsRequest)request);
    }
}
