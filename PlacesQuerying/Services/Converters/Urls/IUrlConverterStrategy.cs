﻿using Flurl;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls
{
    public interface IUrlConverterStrategy<TRequest> : IUrlConverterStrategyBase
    {
        Url ToUrl(Url baseEndpoint, TRequest request);
    }
}
