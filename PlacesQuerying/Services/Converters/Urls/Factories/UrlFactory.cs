﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.Factories
{
    public class UrlFactory : IUrlFactory
    {
        private IEnumerable<IUrlConverterStrategyBase> _urlConverterStrategies;

        public UrlFactory(IEnumerable<IUrlConverterStrategyBase> urlConverterStrategies)
        {
            _urlConverterStrategies = urlConverterStrategies;
        }

        public string GetUrl<TRequest>(string baseEndpoint, TRequest request) =>
            _urlConverterStrategies
                .OfType<IUrlConverterStrategy<TRequest>>()
                .First()
                .ToUrl(baseEndpoint, request);
    }
}
