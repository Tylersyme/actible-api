﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.Factories
{
    public interface IUrlFactory
    {
        string GetUrl<TRequest>(string baseEndpoint, TRequest request);
    }
}
