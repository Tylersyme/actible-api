﻿using Flurl;
using PlacesQuerying.Requests.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.TomTom
{
    internal class TomTomNearbyRequestUrlConverter : TomTomSearchRequestUrlConverter<TomTomNearbySearchRequest>
    {
        public override Url ToUrl(Url baseEndpoint, TomTomNearbySearchRequest request)
        {
            var url = base.ToUrl(baseEndpoint, request);

            url = url
                .SetQueryParams(new
                {
                    radius = request.Radius,
                    limit = request.ResultLimit,
                });

            return url;
        }
    }
}
