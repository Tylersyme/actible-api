﻿using Autofac.Builder;
using Flurl;
using PlacesQuerying.Requests.TomTom.Search;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace PlacesQuerying.Services.Converters.Urls.TomTom
{
    internal abstract class TomTomSearchRequestUrlConverter<TRequest> : IUrlConverterStrategy<TRequest> where TRequest : TomTomSearchRequest
    {
        private const string SEARCH_ENDPOINT = "search";

        public virtual Url ToUrl(Url baseEndpoint, TRequest searchRequest)
        {
            var url = baseEndpoint
                .AppendPathSegment(SEARCH_ENDPOINT)
                .AppendPathSegment(searchRequest.VersionNumber.ToString())
                .AppendPathSegment(searchRequest.SearchMethod.UrlSegment)
                .AppendPathSegment($".{searchRequest.ResponseFormat.Name}")
                .SetQueryParams(new
                {
                    key = searchRequest.ApiKey,
                    lat = searchRequest.Latitude,
                    lon = searchRequest.Longitude,
                    categorySet = searchRequest.CategorySet,
                });

            return url;
        }

        public Url ToUrl(Url baseEndpoint, object request) =>
            this.ToUrl(baseEndpoint, (TRequest)request);
    }
}
