﻿using PlacesQuerying.Enums;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace PlacesQuerying.Services.Converters.Json
{
    public class TomTomCategoryCodeConverter : JsonConverter<TomTomCategoryCode>
    {
        public override TomTomCategoryCode Read(ref Utf8JsonReader reader, Type typeToConvert, JsonSerializerOptions options)
        {
            var code = reader.GetString();

            return TomTomCategoryCode.GetByStringOrUnsupported(code);
        }

        public override void Write(Utf8JsonWriter writer, TomTomCategoryCode value, JsonSerializerOptions options)
        {
            throw new NotImplementedException();
        }
    }
}
