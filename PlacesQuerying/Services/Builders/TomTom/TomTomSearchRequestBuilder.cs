﻿using PlacesQuerying.Enums;
using PlacesQuerying.Requests.TomTom.Search;
using PlacesQuerying.Services.Builders.TomTom.NearbySearch;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Builders.TomTom
{
    internal class TomTomSearchRequestBuilder<TBuilder, TRequest> : ITomTomSearchRequestBuilder<TBuilder>
        where TBuilder : TomTomSearchRequestBuilder<TBuilder, TRequest>
        where TRequest : TomTomSearchRequest, new()
    {
        // TODO: Move API key out of code
        private const string API_KEY = "EhcXdIz9FGfJMGbbRyT12y2zZ7jiE8ki";

        private const int TOM_TOM_DEFAULT_VERSION_NUMBER = 2;

        private TRequest _searchRequest;
        protected TRequest SearchRequest => _searchRequest;

        public TBuilder WithCategoryFilter(TomTomCategoryCode categoryCode)
        {
            if (!string.IsNullOrWhiteSpace(_searchRequest.CategorySet))
                _searchRequest.CategorySet += ",";

            _searchRequest.CategorySet += categoryCode.Id;

            return (TBuilder)this;
        }

        protected TRequest CreateNewRequest(float latitude, float longitude)
        {
            _searchRequest = new TRequest();
            _searchRequest.Latitude = latitude;
            _searchRequest.Longitude = longitude;

            this.SetSearchRequestDefaults(_searchRequest);

            return _searchRequest;
        }

        private void SetSearchRequestDefaults(TRequest searchRequest)
        {
            searchRequest.VersionNumber = TOM_TOM_DEFAULT_VERSION_NUMBER;
            searchRequest.ApiKey = API_KEY;
            searchRequest.ResponseFormat = TomTomResponseFormat.Json;
        }
    }
}
