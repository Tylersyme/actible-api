﻿using PlacesQuerying.Enums;
using PlacesQuerying.Services.Builders.TomTom.NearbySearch;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Builders.TomTom
{
    public interface ITomTomSearchRequestBuilder<TBuilder>
        where TBuilder : ITomTomSearchRequestBuilder<TBuilder>
    {
        TBuilder WithCategoryFilter(TomTomCategoryCode categoryCode);
    }
}
