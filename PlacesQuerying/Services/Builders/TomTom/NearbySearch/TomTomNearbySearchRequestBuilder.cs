﻿using PlacesQuerying.Enums;
using PlacesQuerying.Requests.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Builders.TomTom.NearbySearch
{
    internal class TomTomNearbySearchRequestBuilder : TomTomSearchRequestBuilder<TomTomNearbySearchRequestBuilder, TomTomNearbySearchRequest>, ITomTomNearbySearchRequestBuilder
    {
        public ITomTomNearbySearchRequestBuilder Create(float latitude, float longitude)
        {
            base.CreateNewRequest(latitude, longitude);

            return this;
        }

        public ITomTomNearbySearchRequestBuilder WithRadius(int radius)
        {
            base.SearchRequest.Radius = radius;

            return this;
        }

        public ITomTomNearbySearchRequestBuilder WithResultLimit(int resultLimit)
        {
            base.SearchRequest.ResultLimit = resultLimit;

            return this;
        }

        public ITomTomNearbySearchRequestBuilder WithResultOffset(int resultOffset)
        {
            base.SearchRequest.ResultOffset = resultOffset;

            return this;
        }

        public TomTomNearbySearchRequest Build() =>
            base.SearchRequest;

        ITomTomNearbySearchRequestBuilder ITomTomSearchRequestBuilder<ITomTomNearbySearchRequestBuilder>.WithCategoryFilter(TomTomCategoryCode categoryCode) =>
            base.WithCategoryFilter(categoryCode);
    }
}
