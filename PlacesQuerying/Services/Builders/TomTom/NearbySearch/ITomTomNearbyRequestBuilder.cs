﻿using PlacesQuerying.Requests.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Builders.TomTom.NearbySearch
{
    public interface ITomTomNearbySearchRequestBuilder : ITomTomSearchRequestBuilder<ITomTomNearbySearchRequestBuilder>
    {
        ITomTomNearbySearchRequestBuilder Create(float latitude, float longitude);

        ITomTomNearbySearchRequestBuilder WithRadius(int radius);
        ITomTomNearbySearchRequestBuilder WithResultLimit(int resultLimit);
        ITomTomNearbySearchRequestBuilder WithResultOffset(int resultOffset);

        TomTomNearbySearchRequest Build();
    }
}
