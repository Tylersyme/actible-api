﻿using System;
using System.Collections.Generic;
using System.Text;

namespace PlacesQuerying.Services.Builders.TomTom.Interfaces
{
    public interface ITomTomBuildable<TRequest>
    {
        string Build(TRequest request);
    }
}
