﻿using PlacesQuerying.Requests.Yelp.Category;
using PlacesQuerying.Requests.Yelp.Search;
using PlacesQuerying.Responses.Yelp.Category;
using PlacesQuerying.Responses.Yelp.Search;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlacesQuerying.Services.Clients
{
    public interface IYelpClient
    {
        Task<YelpBusinessSearchResponse> BusinessSearchAsync(YelpBusinessSearchRequest request);

        Task<YelpBusinessMatchResponse> BusinessMatchAsync(YelpBusinessMatchRequest request);

        Task<YelpBusinessDetailsResponse> BusinessDetailsAsync(YelpBusinessDetailsRequest request);

        Task<YelpAllCategoriesResponse> GetAllCategoriesAsync(YelpAllCategoriesRequest request);
    }
}
