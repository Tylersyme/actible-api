﻿using PlacesQuerying.Exceptions;
using PlacesQuerying.Requests.Yelp.Category;
using PlacesQuerying.Requests.Yelp.Search;
using PlacesQuerying.Responses.Yelp.Category;
using PlacesQuerying.Responses.Yelp.Search;
using PlacesQuerying.Services.Converters.Urls.Factories;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace PlacesQuerying.Services.Clients
{
    public class YelpClient : IYelpClient
    {
        private const string BASE_ENDPOINT = "https://api.yelp.com";

        private readonly HttpClient _httpClient;

        private readonly IUrlFactory _urlFactory;

        public YelpClient(IUrlFactory urlFactory)
        {
            _httpClient = new HttpClient();

            _urlFactory = urlFactory;
        }

        private async Task<TResponse> GetResponse<TRequest, TResponse>(TRequest request, string apiKey)
        {
            var requestUrl = _urlFactory.GetUrl(BASE_ENDPOINT, request);

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiKey);

            var response = await _httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<TResponse>();

            return content;
        }

        public async Task<YelpBusinessSearchResponse> BusinessSearchAsync(YelpBusinessSearchRequest request) =>
            await this.GetResponse<YelpBusinessSearchRequest, YelpBusinessSearchResponse>(request, request.ApiKey);

        public async Task<YelpBusinessMatchResponse> BusinessMatchAsync(YelpBusinessMatchRequest request) =>
            await this.GetResponse<YelpBusinessMatchRequest, YelpBusinessMatchResponse>(request, request.ApiKey);

        public async Task<YelpBusinessDetailsResponse> BusinessDetailsAsync(YelpBusinessDetailsRequest request) =>
            await this.GetResponse<YelpBusinessDetailsRequest, YelpBusinessDetailsResponse>(request, request.ApiKey);

        public async Task<YelpAllCategoriesResponse> GetAllCategoriesAsync(YelpAllCategoriesRequest request) =>
            await this.GetResponse<YelpAllCategoriesRequest, YelpAllCategoriesResponse>(request, request.ApiKey);
    }
}
