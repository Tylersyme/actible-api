﻿using PlacesQuerying.Requests.TomTom.Search;
using PlacesQuerying.Responses.TomTom.Search;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace PlacesQuerying.Services.Clients
{
    public interface ITomTomClient
    {
        Task<TomTomNearbySearchResponse> NearbySearchAsync(TomTomNearbySearchRequest request);
    }
}
