﻿using Flurl;
using PlacesQuerying.Exceptions;
using PlacesQuerying.Requests.TomTom.Search;
using PlacesQuerying.Responses.TomTom.Search;
using PlacesQuerying.Services.Converters;
using PlacesQuerying.Services.Converters.Urls;
using PlacesQuerying.Services.Converters.Urls.Factories;
using PlacesQuerying.Services.Converters.Urls.TomTom;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Http.Json;
using System.Text;
using System.Threading.Tasks;

namespace PlacesQuerying.Services.Clients
{
    public class TomTomClient : ITomTomClient
    {
        private const string BASE_ENDPOINT = "https://api.tomtom.com";

        private readonly HttpClient _httpClient;

        private readonly IUrlFactory _urlFactory;

        public TomTomClient(IUrlFactory urlFactory)
        {
            _httpClient = new HttpClient();

            _urlFactory = urlFactory;
        }

        public async Task<TomTomNearbySearchResponse> NearbySearchAsync(TomTomNearbySearchRequest request)
        {
            var requestUrl = _urlFactory.GetUrl(BASE_ENDPOINT, request);

            _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", request.ApiKey);

            var response = await _httpClient.GetAsync(requestUrl);

            if (!response.IsSuccessStatusCode)
                throw new ApiRequestException();

            var content = await response.Content.ReadFromJsonAsync<TomTomNearbySearchResponse>();

            return content;
        }
    }
}
