﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.Models
{
    public class AuthenticatedUser
    {
        public string Token { get; set; }

        public Guid UserId { get; set; }
    }
}
