﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication
{
    public class RegistrationData
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string PlainTextPassword { get; set; }
    }
}
