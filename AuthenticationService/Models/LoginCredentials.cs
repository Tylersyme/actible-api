﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.Models
{
    public class LoginCredentials
    {
        public string Email { get; set; }

        public string Username { get; set; }

        public string PlainTextPassword { get; set; }
    }
}
