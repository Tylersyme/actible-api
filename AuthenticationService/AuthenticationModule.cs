﻿using Authentication.Services;
using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication
{
    public class AuthenticationModule : Module
    {
        protected override void Load(ContainerBuilder builder)
        {
            builder
                .RegisterType<RegisterService>()
                .As<IRegisterService>();

            builder
                .RegisterType<LoginService>()
                .As<ILoginService>();

            builder
                .RegisterType<PasswordService>()
                .As<IPasswordService>();
        }
    }
}
