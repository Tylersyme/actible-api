﻿using AutoMapper;
using Entities.Authentication;
using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.MappingProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            base.CreateMap<RegistrationData, User>();
        }
    }
}
