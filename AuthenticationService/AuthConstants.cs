﻿using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication
{
    // TODO: Move to app settings
    public static class AuthConstants
    {
        public const string Issuer = Audience;
        public const string Audience = "https://localhost:5004/";
        // TODO: Inject on deployment through app settings
        public const string Secret = "1178fc26-590e-4e7b-a878-e52bf9daca5d";
    }
}
