﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.Services
{
    public class PasswordService : IPasswordService
    {
        public byte[] Hash(string plainTextPassword) =>
            Encoding.ASCII.GetBytes(BCrypt.Net.BCrypt.HashPassword(plainTextPassword));

        public bool Verify(string plainTextPassword, byte[] hashedPassword) =>
            BCrypt.Net.BCrypt.Verify(plainTextPassword, Encoding.ASCII.GetString(hashedPassword));
    }
}
