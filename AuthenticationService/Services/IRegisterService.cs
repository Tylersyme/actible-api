﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.Services
{
    public interface IRegisterService
    {
        Task TryRegisterOrThrowAsync(RegistrationData registrationData);
    }
}
