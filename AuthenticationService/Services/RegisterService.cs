﻿using Authentication.Exceptions;
using AutoMapper;
using Entities.Authentication;
using Persistence;
using Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Authentication.Services
{
    public class RegisterService : IRegisterService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordService _passwordService;
        private readonly IMapper _mapper;

        public RegisterService(
            IUserRepository userRepository,
            IPasswordService passwordService,
            IMapper mapper)
        {
            _userRepository = userRepository;
            _passwordService = passwordService;
            _mapper = mapper;
        }

        public async Task TryRegisterOrThrowAsync(RegistrationData registrationData)
        {
            if (!await _userRepository.IsUsernameUniqueAsync(registrationData.Username))
                throw new UsernameNotUniqueException();

            var user = _mapper.Map<User>(registrationData);
            user.HashedPassword = _passwordService.Hash(registrationData.PlainTextPassword);

            await _userRepository.CreateUserAsync(user);
        }
    }
}
