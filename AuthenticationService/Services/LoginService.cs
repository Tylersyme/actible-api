﻿using Authentication.Exceptions;
using Authentication.Models;
using Microsoft.IdentityModel.Tokens;
using Microsoft.VisualBasic;
using Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Services
{
    public class LoginService : ILoginService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordService _passwordService;

        public LoginService(
            IUserRepository userRepository, 
            IPasswordService passwordService)
        {
            _userRepository = userRepository;
            _passwordService = passwordService;
        }

        public async Task<AuthenticatedUser> TryLoginOrThrowAsync(LoginCredentials loginCredentials)
        {
            var user = await _userRepository.GetUserByUsernameAsync(loginCredentials.Username);

            if (user == null)
                throw new InvalidLoginCredentialsException();

            var isPasswordCorrect = _passwordService.Verify(loginCredentials.PlainTextPassword, user.HashedPassword);

            if (!isPasswordCorrect)
                throw new InvalidLoginCredentialsException();

            var tokenHandler = new JwtSecurityTokenHandler();

            var claims = new Claim[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, user.Id.ToString()),
            };

            var secretKeyBytes = Encoding.UTF8.GetBytes(AuthConstants.Secret);
            var key = new SymmetricSecurityKey(secretKeyBytes);

            var signingCredentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha256);

            var token = new JwtSecurityToken(
                AuthConstants.Issuer,
                AuthConstants.Audience,
                claims,
                notBefore: DateTime.UtcNow,
                expires: DateTime.UtcNow.AddDays(3),
                signingCredentials
            );

            var jsonToken = tokenHandler.WriteToken(token);

            return new AuthenticatedUser
            {
                Token = jsonToken,
                UserId = user.Id,
            };
        }
    }
}
