﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Authentication.Services
{
    public interface IPasswordService
    {
        byte[] Hash(string plainTextPassword);

        bool Verify(string plainTextPassword, byte[] hashedPassword);
    }
}
