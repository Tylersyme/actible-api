﻿using Authentication.Models;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Authentication.Services
{
    public interface ILoginService
    {
        Task<AuthenticatedUser> TryLoginOrThrowAsync(LoginCredentials loginCredentials);
    }
}
